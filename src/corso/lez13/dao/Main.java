package corso.lez13.dao;

import java.sql.SQLException;

import corso.lez13.dao.models.dao.PersonaDao;
import corso.lez13.mvc.model.Persona;

public class Main {

	public static void main(String[] args) {

		PersonaDao daoPer = new PersonaDao();
		
		Persona perUno = new Persona();
		perUno.setPer_nome("Marika");
		perUno.setPer_cogn("Akiram");
		perUno.setPer_tele("789456");
		perUno.setPer_codf("MARAKI");
		
		try {
			daoPer.save(perUno);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		Persona perDue = new Persona();
		perDue.setPer_nome("Giovanni");
		perDue.setPer_cogn("Pace");
		perDue.setPer_tele("123456");
		perDue.setPer_codf("PCAGNN");

		try {
			daoPer.save(perDue);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
	}

}
