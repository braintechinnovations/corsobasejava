package corso.lez13.dao.models.db;

import java.sql.SQLException;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class ConnettoreDB {

	private Connection conn;						
	private static ConnettoreDB ogg_connessione;	
	
	public static ConnettoreDB getInstance() {
		if(ogg_connessione == null) {	
			ogg_connessione = new ConnettoreDB();
		}
		
		return ogg_connessione;
	}
	
	public Connection getConnection() throws SQLException {
		
		if(conn == null) {			
			MysqlDataSource dataSource = new MysqlDataSource();
			dataSource.setServerName("localhost");	//Oppure 127.0.0.1
			dataSource.setPortNumber(3306);
			dataSource.setUser("root");
			dataSource.setPassword("toor");
			dataSource.setDatabaseName("rubrica");
			dataSource.setUseSSL(false);
			
			conn = (Connection) dataSource.getConnection();
		}
		
		return conn;
			
	}
	
}
