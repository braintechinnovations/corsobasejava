package corso.lez13.dao.models.dao;

import java.sql.SQLException;
import java.util.ArrayList;

public interface Dao<T> {	//Al posto di T (quando mi serve) comparirÓ la classe da utilizzare

	void save(T t) throws SQLException;
	
	boolean delete(T t) throws SQLException;
	
	boolean update(T t) throws SQLException;
	
	ArrayList<T> findAll() throws SQLException;

	T findById(int id) throws SQLException;
}
