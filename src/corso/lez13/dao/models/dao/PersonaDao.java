package corso.lez13.dao.models.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

import corso.lez13.mvc.model.Persona;
import corso.lez13.mvc.model.db.ConnettoreDB;

public class PersonaDao implements Dao<Persona> {

	@Override
	public void save(Persona t) throws SQLException {
		ConnettoreDB singleton = ConnettoreDB.getInstance();
		Connection conn = singleton.getConnection();

		String queryInserimento = "INSERT INTO persona "
				+ "(nome, cognome, telefono, codice_fiscale) VALUES (?,?,?,?)"; 
		
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(queryInserimento, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, t.getPer_nome());	
		ps.setString(2, t.getPer_cogn());
		ps.setString(3, t.getPer_tele());
		ps.setString(4, t.getPer_codf());
		
		ps.executeUpdate();
		ResultSet rs = ps.getGeneratedKeys();
		
		rs.next();
		int idGenerato = rs.getInt(1);
		t.setPer_id(idGenerato);
	}

	@Override
	public boolean delete(Persona t) throws SQLException {
		ConnettoreDB singleton = ConnettoreDB.getInstance();
		Connection conn = singleton.getConnection();
		
		String queryEliminazione = "DELETE FROM persona WHERE personaId = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(queryEliminazione);
		ps.setInt(1, t.getPer_id());

		int affRows = ps.executeUpdate();
		if(affRows > 0) {
			return true;
		}
		
		return false;
	}

	@Override
	public boolean update(Persona t) throws SQLException {
		ConnettoreDB singleton = ConnettoreDB.getInstance();
		Connection conn = singleton.getConnection();
		
		String queryModifica = "UPDATE persona SET "
												+ "nome = ?, "
												+ "cognome = ?, "
												+ "telefono = ?, "
												+ "codice_fiscale = ? "
												+ "WHERE personaId = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(queryModifica);
		ps.setString(1, t.getPer_nome());
		ps.setString(2, t.getPer_cogn());
		ps.setString(3, t.getPer_tele());
		ps.setString(4, t.getPer_codf());
		ps.setInt(5, t.getPer_id());
		
		int affRows = ps.executeUpdate();
		if(affRows > 0)
			return true;			//Una sola condizione nella if SENZA le { }
		
		return false;
	}

	@Override
	public ArrayList<Persona> findAll() throws SQLException {

		ArrayList<Persona> elenco = new ArrayList<Persona>();
		ConnettoreDB singleton = ConnettoreDB.getInstance();
		Connection conn = singleton.getConnection();
		
		String queryRicerca = "SELECT personaId, nome, cognome, "
				+ "telefono, codice_fiscale FROM persona";
		
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(queryRicerca);
		
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			Persona temp = new Persona();
			temp.setPer_id(rs.getInt(1));
			temp.setPer_nome(rs.getString(2));
			temp.setPer_cogn(rs.getString(3));
			temp.setPer_tele(rs.getString(4));
			temp.setPer_codf(rs.getString(5));
			
			elenco.add(temp);
		}
		
		return elenco;
	}

	@Override
	public Persona findById(int id) throws SQLException {
		ConnettoreDB singleton = ConnettoreDB.getInstance();
		Connection conn = singleton.getConnection();
		
		String queryRicerca = "SELECT personaId, nome, cognome, "
				+ "telefono, codice_fiscale FROM persona WHERE personaId = ?";
		
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(queryRicerca);
		ps.setInt(1, id);
		
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {						//Se ci sono 0 risultati, rs.next() sar� FALSE
			Persona temp = new Persona();
			temp.setPer_id(rs.getInt(1));
			temp.setPer_nome(rs.getString(2));
			temp.setPer_cogn(rs.getString(3));
			temp.setPer_tele(rs.getString(4));
			temp.setPer_codf(rs.getString(5));
			
			return temp;
		}
		
		return null;
	}

	
}
