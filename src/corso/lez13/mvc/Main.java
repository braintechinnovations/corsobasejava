package corso.lez13.mvc;

import java.sql.SQLException;

import corso.lez13.mvc.model.Persona;
import corso.lez13.mvc.model.crud.GestorePersona;

public class Main {

	public static void main(String[] args) {


		GestorePersona gestore = new GestorePersona();
		
		Persona perUno = new Persona();
		perUno.setPer_nome("Marika");
		perUno.setPer_cogn("Akiram");
		perUno.setPer_tele("789456");
		perUno.setPer_codf("MARAKI");
		
		try {
			gestore.inserisciPersona(perUno);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

		Persona perDue = new Persona();
		perDue.setPer_nome("Giovanni");
		perDue.setPer_cogn("Pace");
		perDue.setPer_tele("123456");
		perDue.setPer_codf("PCAGNN");

		try {
			gestore.inserisciPersona(perDue);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

}
