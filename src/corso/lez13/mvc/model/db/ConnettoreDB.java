package corso.lez13.mvc.model.db;

import java.sql.SQLException;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class ConnettoreDB {

	private Connection conn;						//Connessione che non viene dichiarata statica perch� di propriet� dell'oggetto che effettivamente � stato dichiarato statico!
	private static ConnettoreDB ogg_connessione;	//Variabile dove vado a salvare in modalit� statica l'oggetto Connettore che poi verr� passato come la singola istanza a chi lo desidera!
	
	/**
	 * Metodo per la restituzione della singola istanza Singleton che non viene ricreata se gi� esiste!
	 * @return
	 */
	public static ConnettoreDB getInstance() {
		if(ogg_connessione == null) {				//Se non esiste l'istanza
			ogg_connessione = new ConnettoreDB();	//Creo l'istanza e la salvo all'interno della variabile statica
		}
		
		return ogg_connessione;						//Restituisco l'istanza, se esiste non la ricreo (riga 14)
	}
	
	/**
	 * Metodo per la restituizione della connessione, se esiste non  la ricreo!
	 * @return	Connection
	 * @throws SQLException
	 */
	public Connection getConnection() throws SQLException {
		
		if(conn == null) {			
			MysqlDataSource dataSource = new MysqlDataSource();
			dataSource.setServerName("localhost");	//Oppure 127.0.0.1
			dataSource.setPortNumber(3306);
			dataSource.setUser("root");
			dataSource.setPassword("toor");
			dataSource.setDatabaseName("rubrica");
			dataSource.setUseSSL(false);
			
			conn = (Connection) dataSource.getConnection();
		}
		
		return conn;
			
	}
	
}
