package corso.lez13.mvc.model;

public class Persona {

	int per_id;
	String per_nome;
	String per_cogn;
	String per_tele;
	String per_codf;
	
	public Persona() {
		
	}
	
	public Persona(int per_id, String per_nome, String per_cogn, String per_tele, String per_codf) {
		super();
		this.per_id = per_id;
		this.per_nome = per_nome;
		this.per_cogn = per_cogn;
		this.per_tele = per_tele;
		this.per_codf = per_codf;
	}



	public int getPer_id() {
		return per_id;
	}
	public void setPer_id(int per_id) {
		this.per_id = per_id;
	}
	public String getPer_nome() {
		return per_nome;
	}
	public void setPer_nome(String per_nome) {
		this.per_nome = per_nome;
	}
	public String getPer_cogn() {
		return per_cogn;
	}
	public void setPer_cogn(String per_cogn) {
		this.per_cogn = per_cogn;
	}
	public String getPer_tele() {
		return per_tele;
	}
	public void setPer_tele(String per_tele) {
		this.per_tele = per_tele;
	}
	public String getPer_codf() {
		return per_codf;
	}
	public void setPer_codf(String per_codf) {
		this.per_codf = per_codf;
	}

	@Override
	public String toString() {
		return "Persona [per_id=" + per_id + ", per_nome=" + per_nome + ", per_cogn=" + per_cogn + ", per_tele="
				+ per_tele + ", per_codf=" + per_codf + "]";
	}
	
}
