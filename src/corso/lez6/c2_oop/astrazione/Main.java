package corso.lez6.c2_oop.astrazione;

public class Main {

	public static void main(String[] args) {

//		Animale aniUno = new Animale();
		
		Gatto anakin = new Gatto();
		anakin.setNumZampe(4);
		anakin.setSesso("M");
		anakin.versoEmesso();
		
		Pesce nemo = new Pesce();
		nemo.setNumZampe(0);
		nemo.setSesso("M");
		nemo.setHasScaglie(true);
		nemo.versoEmesso();
		
		
		//Workaround che mi permette di inizializzare un oggetto di tipo "animale" anche se effettivamente � pi� simile ad un'idea
		Animale aniUno = new Animale() {
			
			@Override
			public void versoEmesso() {
				System.out.println("T'ho fregato!");
			}
		};
		
		aniUno.versoEmesso();
		
	}

}
