package corso.lez6.c2_oop.astrazione;

public abstract class Animale {

	private int numZampe;
	private String sesso;
	private boolean hasPiume;
	private boolean hasScaglie;
	
	public abstract void versoEmesso();
	
	public int getNumZampe() {
		return numZampe;
	}
	public void setNumZampe(int numZampe) {
		this.numZampe = numZampe;
	}
	public String getSesso() {
		return sesso;
	}
	public void setSesso(String sesso) {
		this.sesso = sesso;
	}
	public boolean isHasPiume() {
		return hasPiume;
	}
	public void setHasPiume(boolean hasPiume) {
		this.hasPiume = hasPiume;
	}

	public boolean isHasScaglie() {
		return hasScaglie;
	}

	public void setHasScaglie(boolean hasScaglie) {
		this.hasScaglie = hasScaglie;
	}
	
	
	
	
	
	
}
