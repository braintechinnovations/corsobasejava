package corso.lez6.c3_oop.interfaccia;

public class Gatto implements Animale {

	private boolean hasPelo;
	private String nome;
	private String sesso;
	
	@Override
	public void versoEmesso() {

		System.out.println("Miau miau!");
	}

	@Override
	public void metodoLocomozione() {

		System.out.println("Cammina");
	}

	public boolean getHasPelo() {
		return hasPelo;
	}

	public void setHasPelo(boolean hasPelo) {
		this.hasPelo = hasPelo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSesso() {
		return sesso;
	}

	public void setSesso(String sesso) {
		this.sesso = sesso;
	}
	
	

}
