package corso.lez6.c4_oop.polimorfismo;

public class Giocattolo extends Articolo{

	private int etaMinima;
	
	public Giocattolo() {
		
	}

	public int getEtaMinima() {
		return etaMinima;
	}

	public void setEtaMinima(int etaMinima) {
		this.etaMinima = etaMinima;
	}
	
	@Override
	public void stampa() {
		String frase = "GIOCATTOLO:\n"
				+ "Codice: " + super.codice + "\n"
				+ "Prezzo: " + super.prezzo + "\n"
				+ "Et� minima: " + this.etaMinima + "\n";
		
		System.out.println(frase);
	}
	

	public void stampaCsvGiocattolo() {

		String frase = "QUOTIDIANO:" + super.codice + ","
				+ super.prezzo + "," + this.etaMinima + "\n";
		
		System.out.println(frase);
	}
	
}
