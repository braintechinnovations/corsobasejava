package corso.lez6.c4_oop.polimorfismo;

public class Rivista extends Articolo{

	private String titolo;
	private String casaEditrice;
	
	public Rivista() {
		
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public String getCasaEditrice() {
		return casaEditrice;
	}

	public void setCasaEditrice(String casaEditrice) {
		this.casaEditrice = casaEditrice;
	}
	
	@Override
	public void stampa() {
		String frase = "RIVISTA:\n"
				+ "Codice: " + super.codice + "\n"
				+ "Prezzo: " + super.prezzo + "\n"
				+ "Titolo: " + this.titolo + "\n"
				+ "Casa Editrice: " + this.casaEditrice + "\n";
		
		System.out.println(frase);
	}
	
	public void stampaCsvRivista() {

		String frase = "RIVISTA:" + super.codice + ","
				+ super.prezzo + "," + this.titolo + ","
				+ this.casaEditrice + "\n";
		
		System.out.println(frase);
	}
	
	
	
}
