package corso.lez6.c4_oop.polimorfismo.hw;

public abstract class Articolo {

	private String codice;
	private String nome;
	private float prezzo;
	
	public String Codice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public float getPrezzo() {
		return prezzo;
	}
	public void setPrezzo(float prezzo) {
		this.prezzo = prezzo;
	}
}
