package corso.lez6.c4_oop.polimorfismo.hw;

public class Bevanda extends Articolo{

	private float volume; 
	
	public Bevanda() {
		
	}
	
	public Bevanda(String codice, String nome, float prezzo, float volume) {
		super.setCodice(codice);
		super.setNome(nome);
		super.setPrezzo(prezzo);
		this.volume = volume;
	}

	public float getVolume() {
		return volume;
	}

	public void setVolume(float volume) {
		this.volume = volume;
	}	
}
