package corso.lez6.c4_oop.polimorfismo.hw;

public class Piatto extends Articolo{

	private boolean isVegano;
	
	public Piatto() {
		
	}
	
	public Piatto(String codice, String nome, float prezzo, boolean isVegano) {
		super.setCodice(codice);
		super.setNome(nome);
		super.setPrezzo(prezzo);
		this.isVegano = isVegano;
	}

	public boolean isVegano() {
		return isVegano;
	}

	public void setVegano(boolean isVegano) {
		this.isVegano = isVegano;
	}
	
	
	
}
