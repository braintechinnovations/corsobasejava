package corso.lez6.c4_oop.polimorfismo.hw;

import java.util.ArrayList;

public class Ristorante {

	private String nome;
	private ArrayList<Articolo> menu = new ArrayList<Articolo>();
	
	public Ristorante(String nome) {
		this.nome = nome;
	}
	
	public void addArticolo(Articolo objArt) {
		this.menu.add(objArt);
	}
	
	public void creaArticolo(
			String tipo, 
			String codice, 
			String nome, 
			float prezzo, 
			boolean isVegano,
			float volume) {
		
		switch(tipo) {
			case "PIATTO":
				Piatto tempPiatto = new Piatto(codice, nome, prezzo, isVegano);
				this.menu.add(tempPiatto);
				break;
			case "BEVANDA":
				Bevanda tempBevanda = new Bevanda(codice, nome, prezzo, volume);
				this.menu.add(tempBevanda);
				break;
		}
		
	}
}
