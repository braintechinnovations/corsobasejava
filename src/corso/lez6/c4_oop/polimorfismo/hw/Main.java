package corso.lez6.c4_oop.polimorfismo.hw;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {

		/*
		 * Creare un sistema di gestione menu per uno o pi� ristoranti
		 * Ogni ristorante avr� due categorie di articoli: bevande, piatti
		 * 
		 * Creare dei metodi in grado di:
		 * - Inserire una bevanda o un piatto
		 * - Stampare tutte le bevande o piatti (a scelta)
		 * - HARD: Contare tutte le bevande o piatti o entrambi
		 * 
		 * Ogni articolo avr� ALMENO:
		 * - Codice
		 * - Nome
		 * - Prezzo
		 * 
		 * 1. Fatevi la classe Articolo
		 * 2. Fatevi le classi che estendono Articolo
		 * 3. Negozio che avr� al suo interno un elenco di articoli che sar� effettivamente il menu!
		 */
		
		Piatto noodles = new Piatto("ND1234", "Noodles vegani", 3.50f, true);
		Piatto ravioli = new Piatto("RV1234", "Ravioli alla griglia", 4.0f, false);
		
		Bevanda colaPiccola = new Bevanda("CC1234", "Coca cola", 2.5f, 0.25f);
		Bevanda colaGrande = new Bevanda("CC1235", "Coca cola", 3.5f, 0.4f);
		
		Articolo birra = new Bevanda("BI1234", "Birra alla spina", 5.0f, 0.5f);	//Binding statico
		
//		ArrayList<Articolo> elenco = new ArrayList<Articolo>();
//		elenco.add(noodles);
//		elenco.add(colaGrande);
		
		Ristorante newChina = new Ristorante("New China");
		newChina.addArticolo(noodles);
		newChina.addArticolo(ravioli);
		newChina.addArticolo(colaPiccola);
		newChina.addArticolo(colaGrande);
		
		newChina.creaArticolo("PIATTO", "GN-1234", "Gnocchi cinesi", 2.4f, false, 0);
	}

}
