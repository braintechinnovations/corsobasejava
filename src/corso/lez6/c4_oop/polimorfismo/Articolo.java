package corso.lez6.c4_oop.polimorfismo;

public abstract class Articolo {

	protected String codice;
	protected float prezzo;
	
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public float getPrezzo() {
		return prezzo;
	}
	public void setPrezzo(float prezzo) {
		this.prezzo = prezzo;
	}
	
	public void stampa() {
		String frase = "ARTICOLO:\n"
				+ "Codice: " + this.codice + "\n"
				+ "Prezzo: " + this.prezzo + "\n";
		
		System.out.println(frase);
	}
}
