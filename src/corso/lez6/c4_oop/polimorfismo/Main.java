package corso.lez6.c4_oop.polimorfismo;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {

		Rivista rivUno = new Rivista();
		Rivista rivDue = new Rivista();
		
		Quotidiano quoUno = new Quotidiano();
		Quotidiano quoDue = new Quotidiano();
		Quotidiano quoTre = new Quotidiano();
		
		Giocattolo gioUno = new Giocattolo();
		Giocattolo gioDue = new Giocattolo();
		Giocattolo gioTre = new Giocattolo();
		
		ArrayList<Articolo> scaffale = new ArrayList<Articolo>();
		scaffale.add(rivUno);
		scaffale.add(rivDue);
		scaffale.add(quoUno);
		scaffale.add(quoDue);
		scaffale.add(quoTre);
		scaffale.add(gioUno);
		scaffale.add(gioDue);
		scaffale.add(gioTre);
		
		for(int i=0; i<scaffale.size(); i++) {
			Articolo temp = scaffale.get(i);
//			temp.stampa();		//Quesrto � possibile direttamente perch� c'� l'override!
			
			if(temp instanceof Rivista) {
				Rivista tempRivista = (Rivista)temp;
				tempRivista.stampaCsvRivista();
			}
			
			if(temp instanceof Quotidiano) {
				Quotidiano tempQuodiano = (Quotidiano)temp;
				tempQuodiano.stampaCsvQuotidiano();
			}
			
			if(temp instanceof Giocattolo) {
				Giocattolo tempGiocattolo = (Giocattolo)temp;
				tempGiocattolo.stampaCsvGiocattolo();
			}
		}
		
	}

}
