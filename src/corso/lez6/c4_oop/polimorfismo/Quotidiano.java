package corso.lez6.c4_oop.polimorfismo;

public class Quotidiano extends Articolo{

	private String titolo;
	private int numPagine;
	private boolean inserto;
	
	public Quotidiano() {
		
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public int getNumPagine() {
		return numPagine;
	}

	public void setNumPagine(int numPagine) {
		this.numPagine = numPagine;
	}

	public boolean isInserto() {
		return inserto;
	}

	public void setInserto(boolean inserto) {
		this.inserto = inserto;
	}

	@Override
	public void stampa() {
		String frase = "QUOTIDIANO:\n"
				+ "Codice: " + super.codice + "\n"
				+ "Prezzo: " + super.prezzo + "\n"
				+ "Titolo: " + this.titolo + "\n"
				+ "Num Pagine: " + this.numPagine + "\n";
		
		System.out.println(frase);
	}
	
	public void stampaCsvQuotidiano() {

		String frase = "QUOTIDIANO:" + super.codice + ","
				+ super.prezzo + "," + this.titolo + ","
				+ this.numPagine + "\n";
		
		System.out.println(frase);
	}
	
	
}
