package corso.lez6.c1_oop.ereditarieta;

public class Studente extends Persona {
	
	private String matricola;
	
	public Studente() {
		
	}

	public Studente(
			String nome,
			String cognome,
			String indirizzo,
			String matricola) {
		
		super.nome = nome;
		super.cognome = cognome;
		super.indirizzo = indirizzo;
		this.matricola = matricola;
	}



	public String getMatricola() {
		return matricola;
	}

	public void setMatricola(String matricola) {
		this.matricola = matricola;
	}
	
	@Override
	public void stampa() {
		String frase = "STUDENTE:\n"
				+ "Nome: " + nome + "\n"
				+ "Cognome: " + cognome + "\n"
				+ "Indirizzo: " + indirizzo + "\n"
				+ "Matricola: " + matricola;
		
		System.out.println(frase);
	}
	
}
