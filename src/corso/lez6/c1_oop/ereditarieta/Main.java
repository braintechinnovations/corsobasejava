package corso.lez6.c1_oop.ereditarieta;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Persona perUno = new Persona("Giovanni", "Pace", "Via buona, 15");
		perUno.stampa();
		
		
		
		Studente studUno = new Studente();
		studUno.setNome("Giogino");
		studUno.setCognome("Giorgetti");
		studUno.setIndirizzo("Via le mani dal naso");
		
		Studente studDue = new Studente("Mario", 
				"Rossi", "P.zza la bomba e scappa", "AB12345");
		studDue.stampa();
		
		Professore profUno = new Professore();
		profUno.setNome("Grigione");
		profUno.setCognome("Grigetti");
		profUno.setIndirizzo("Via inferno, 14");
		
		profUno.stampa();
	}

}
