package corso.lez6.c1_oop.ereditarieta;

public class Professore extends Persona {

	private String corsoAfferenza;

	public String getCorsoAfferenza() {
		return corsoAfferenza;
	}

	public void setCorsoAfferenza(String corsoAfferenza) {
		this.corsoAfferenza = corsoAfferenza;
	}
	
	
	
}
