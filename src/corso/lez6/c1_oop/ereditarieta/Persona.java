package corso.lez6.c1_oop.ereditarieta;

public class Persona {

	protected String nome;
	protected String cognome;
	protected String indirizzo;
	
	public Persona() {
		
	}
	
	public Persona(String nome, String cognome, String indirizzo) {
		this.nome = nome;
		this.cognome = cognome;
		this.indirizzo = indirizzo;
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getIndirizzo() {
		return indirizzo;
	}
	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	
	public void stampa() {
		String frase = "PERSONA:\n"
				+ "Nome: " + this.nome + "\n"
				+ "Cognome: " + this.cognome + "\n"
				+ "Indirizzo: " + this.indirizzo;
		
		System.out.println(frase);
	}
	
}
