package corso.lez1.c1_helloworld;

public class Hello {

	//Entry Point - Da dove inizia tutto!
	public static void main(String[] args) {

		//Stampo una frase e vado a capo con println
		System.out.println("Hello World!");
		//System.out.println("Exterminate!!!");
		
		System.out.print("Ciao ");		//Questo � un commento in linea
		System.out.print("Sono ");		//Questa funzione fa un print e non va a capo!
		System.out.print("Giovanni");
		
		/*
		 * Questo � un commento multilinea, ci� significa 
		 * che andando a capo, automaticament mi diventa tutto commentato!
		 */
		
		/*
		System.out.print("Ciao ");
		System.out.print("Mario");
		*/
	}

}
