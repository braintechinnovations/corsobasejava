package corso.lez1.c4_controlli;

public class ControlliSemplici {

	public static void main(String[] args) {

		/*
		 * if(condizione){
		 * 		//Blocco di codice in caso positivo
		 * }
		 * else{
		 *		//Blocco di codice in caso negativo
		 * }
		 */
		
//		int eta = 15;
//		
//		if(eta >= 18) {
//			System.out.println("Sei maggiorenne");
//		}
//		else {
//			System.out.println("Sei minorenne");
//		}
//		
//		/*
//		 * La condizione, tra numeri pu� assumere diverse tipologie di comparazione:
//		 * == 	Uguaglianza
//		 * != 	Disuguaglianza
//		 * > 	Maggiore
//		 * >=	Maggiore o Uguale
//		 * <	Minore
//		 * <=	Minore o Uguale
//		 */
//		
//		System.out.println(eta >= 18);	//Restituisce l'esito dell'operazione di comparazione (true/false)
//	
		/* -------------------------------------------------------------- */
		
//		int eta = 15;
//		
//		if(eta < 0) {
//			System.out.println("Errore, et� troppo bassa");
//		}
//		else {
//			
//			if(eta >= 130) {
//				System.out.println("Errore, et� troppo elevata");
//			}
//			else {
//				
//				if(eta >= 18) {
//					System.out.println("Sei maggiorenne");
//				}
//				else {
//					System.out.println("Sei minorenne");
//				}
//				
//			}
//		}
		
		/* ----------------------------- OR ------------------------------- */

//		int eta = -100;
//		
//		//int eta = 2000;
//		//  false  ||  true			= true
//		//	0	   +   1			= 1
//		
//		//int eta = -100
//		//  true   ||   false		= true
//		//	1		+   0			= 1
//		if(eta < 0 || eta >= 130) {
//			System.out.println("ERRORE");
//		}
//		else {
//			
//			if(eta >= 18) {
//				System.out.println("Sei maggiorenne");
//			}
//			else {
//				System.out.println("Sei minorenne");
//			}
//			
//		}
		
		/* ----------------------------- AND ------------------------------ */

		int eta = 2000;
		
		//int eta = 2000
		//	1		*		0			= 0
		
		//int eta = -100
		//	0		*		1			= 0
		if(eta >= 0 && eta < 130) {

			if(eta >= 18) {
				System.out.println("Sei maggiorenne");
			}
			else {
				System.out.println("Sei minorenne");
			}
			
		}
		else {
			System.out.println("ERRORE");
		}
		
		/*
		 *	La AND e OR hanno un comportamento a specchio. L'errore prima (OR) si trova 
		 *	nel ramo positivo della IF e nell'altro caso (AND) si trova nell'ELSE
		 */
	}

}
