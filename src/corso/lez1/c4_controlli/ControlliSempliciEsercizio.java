package corso.lez1.c4_controlli;

public class ControlliSempliciEsercizio {

	public static void main(String[] args) {

		/*
		 * TODO: ESERCIZIO!
		 * Scrivere da zero una funzione che, data la temperatura corporea mi dice se posso entrare 
		 * in un locale stando attenti all'input che non deve essere inferiore di 35� e superiore a 42�
		 * 
		 * La temperatura di entrata � di 37.5�
		 * 
		 * Possibili output sono: "Puoi entrare" | "Non puoi entrare"
		 */
		
		float temperatura = 8000.0f;
		
		if(temperatura >= 35.0f && temperatura <= 42.0f) {
			
			if(temperatura < 37.5f) {
				System.out.println("Puoi entrare");
			}
			else {
				System.out.println("Non puoi entrare");
			}
			
		}
		else {
			System.out.println("ERRORE");
		}
		
		///////////////////////////////////////////////////////
		
		if(temperatura < 35 || temperatura > 42) {
			System.out.println("ERRORE");
		}
		else {
			if(temperatura < 37.5f) {
				System.out.println("Puoi entrare");
			}
			else {
				System.out.println("Non puoi entrare");
			}
		}
		
		/* ------------------------------------------------------- */
		
		/*
		 * 1 = TRUE
		 * 0 = FALSE
		 * 
		 * OR (ovvero somma algebrica)
		 * 
		 * ----- | ----- | RISULTATO
		 *   0   +   0   = 0
		 *   1   +   0   = 1
		 *   0   +   1   = 1
		 *   1   +   1   = 1
		 *   
		 *-----------------------------
		 *   
		 * AND (ovvero prodotto algebrico)
		 * 
		 * ----- | ----- | RISULTATO
		 *   0   *   0   = 0
		 *   1   *   0   = 0
		 *   0   *   1   = 0
		 *   1   *   1   = 1
		 */
		
	}

}
