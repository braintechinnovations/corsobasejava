package corso.lez1.c4_controlli;

public class ControlliSempliciStringhe {

	public static void main(String[] args) {

//		String fraseUno = "Giovanni Pace";
//		String fraseDue = "Giovanni Pace";
//		
//		if(fraseUno.equals(fraseDue)) {
//			System.out.println("SONO UGUALI");
//		}
//		else {
//			System.out.println("SONO DIVERSE");
//		}
		
		/* -------------------TRIM SEQUENZIALE---------------------- */
		
//		String fraseUno = " Giovanni Pace ";
//		String fraseDue = "Giovanni Pace ";
//		
//		if(fraseUno.trim().equals(fraseDue.trim())) {
//			System.out.println("SONO UGUALI");
//		}
//		else {
//			System.out.println("SONO DIVERSE");
//		}
		
		/* -------------------------TRIM CON RIASSEGNAZIONE ------------------------ */
		
//		String fraseUno = " Giovanni Pace ";
//		String fraseDue = "Giovanni Pace ";
//		
//		fraseUno = fraseUno.trim();		//Aggiorno la variabile mettendo nella nuova versione, quella vecchia aggiornata
//		fraseDue = fraseDue.trim();
//		
//		if(fraseUno.equals(fraseDue)) {
//			System.out.println("SONO UGUALI");
//		}
//		else {
//			System.out.println("SONO DIVERSE");
//		}
		
		/* ------------------------COMPARAZIONE---------------------- */
		
//		String fraseUno = "Giovanni Pace";
//		String fraseDue = "GIOvanni Pace";
//		
//		fraseUno = fraseUno.toLowerCase();
//		fraseDue = fraseDue.toLowerCase();
//		
//		if(fraseUno.equals(fraseDue)) {
//			System.out.println("SONO UGUALI");
//		}
//		else {
//			System.out.println("SONO DIVERSE");
//		}
		
		/* ------------------------COMPARAZIONE RAPIDA---------------------- */

//		String fraseUno = "Giovanni Pace";
//		String fraseDue = "GIOvanni Pace";
//		
//		if(fraseUno.equalsIgnoreCase(fraseDue)) {
//			System.out.println("SONO UGUALI");
//		}
//		else {
//			System.out.println("SONO DIVERSE");
//		}
		
		/* --------------- VERSIONE CATTIVA --------------------- */
		
		String fraseUno = "   Giovanni Pace";
		String fraseDue = "GIOvanni Pace ";
		
		fraseUno = fraseUno.trim().toUpperCase();
		fraseDue = fraseDue.trim().toUpperCase();
		
		if(fraseUno.equals(fraseDue)) {
			System.out.println("SONO UGUALI");
		}
		else {
			System.out.println("SONO DIVERSE");
		}
		
	}

}
