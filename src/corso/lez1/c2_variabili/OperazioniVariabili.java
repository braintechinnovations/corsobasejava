package corso.lez1.c2_variabili;

public class OperazioniVariabili {

	public static void main(String[] args) {

//		int a = 5;			//Riservo ed assegno un valore all'interno della cella di memoria
//		int b = 15;
//		
//		int c;				//DICHIARAZIONE - Riservo una cella di memoria
//		c = 56;				//ASSEGNAZIONE - Riempio la cella di memoria tramite una assegnazione
//		c = 89;				//Sovrascrittura
//		
//		//int somma = a + b;
//		
//		//sottrazione = a - b; //Non va bene perch� non ho definito il tipo!
//		
//		//int d = 2147483648;		//Errore di sintassi! Vado fuori dai limiti rappresentabili da int.
//	
//		int e = 2147483647;
//		int f = 10;
//		
//		int somma = e + f;
//		System.out.println(somma);
//		
//		//SHORTCUT Ctrl + Maiusc + 7 per commentare/decommentare il codice in maniera rapida!
//		//Ctrl + z per tornare indietro
		
		/* --------------------------- Concatenazione Stringhe - Variabili Intere --------------------------- */
		
//		int a = 5;
//		int b = 15;
//		
//		int somma;
//		somma = a + b;
//		
////		System.out.print("La somma �: ");
////		System.out.println(somma);
//		
////		System.out.println("La somma �: " + somma);		//Concatena una sequenza di caratteri ad una variabile
//		
//		System.out.println("La somma �: " + a + b);		//La somma �: 515
//		System.out.println("La somma �: " + (a + b));	//La somma �: 20
//		System.out.println(a + b + " � la somma!");		//20 � la somma!
		
		/* --------------------------- Tipi di dato primitivi alternativi --------------------- */
		
//		int numIntero = 5;
//		float numDecimaleFloat = 5.4f;
//		double numDecimaleDouble = 5.4d;
//		
//		double numTestUno = 54.6f;			//Un Float pu� essere immagazinato dentro ad un Double
////		float numTestDue = 54.6d;			//Errore, un Double non entra nel Float, � troppo grande!
//		double numTestTre = 1;
		
		
		/* ------------------------------------------------------------------------------------ */

		boolean risultato = true;
		System.out.println(risultato);
		
		risultato = false;
		System.out.println(risultato);
		
	}

}
