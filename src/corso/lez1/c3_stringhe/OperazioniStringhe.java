package corso.lez1.c3_stringhe;

public class OperazioniStringhe {

	public static void main(String[] args) {

		//System.out.println("Sono una stringa");

//		String fraseUno = "Sono una stringa";						//Modo implicito di assegnare una stringa
////		String fraseDue = new String("Sono una nuova Stringa");		//Modo esplicito di assegnare una stringa
//		
//		System.out.println(fraseUno);
		
		/* ---------------------------------------------- */

//		String nome = "Giovanni";
//		String cognome = "Pace";
//		
//		String frase = nome + " " + cognome;
//		System.out.println(frase);
//		System.out.println(frase);
//		System.out.println(frase);				//Effettua 6 operazioni comprese le System...
//		
//		System.out.println("--------");
//		
//		System.out.println(nome + " " + cognome);
//		System.out.println(nome + " " + cognome);
//		System.out.println(nome + " " + cognome);	//Effettuo 9 operazioni
		
		/* ---------------------------------------------- */

		/*
		 * Giovanni Pace � 10 anni vecchio ed ha una temperatura corporea
		 * di 36.6 gradi.
		 * Mario Rossi � 20 anni vecchio ed ha una temperatura corporea
		 * di 35.8 gradi.
		 * 
		 * VARIABILI: nominativo - et� - temperatura
		 */
		
//		String nominativo;
//		int eta;
//		float temperatura;
//		String frase;
//		
//		nominativo = "Giovanni Pace";
//		eta = 10;
//		temperatura = 36.6f;
//		
//		frase = nominativo + " � " + eta + 
//				" anni vecchio ed ha una temperatura corporea di " + temperatura + " gradi";
//		System.out.println(frase);
//		
//		nominativo = "Mario Rossi";
//		eta = 20;
//		temperatura = 35.8f;
//		
//		frase = nominativo + " � " + eta + 
//				" anni vecchio ed ha una temperatura corporea di " + temperatura + " gradi";
//		System.out.println(frase);
		
		/* ---------------------------------------------- */

//		String nome = "                       Giovanni Pace ";
//		
////		System.out.println("La lunghezza del nome �: " + nome.length());
//		
////		System.out.println(nome);			//Ottengo il nome comprensivo di spazi all'inizio ed alla fine
////		System.out.println(nome.trim());	//Rimuove gli spazi all'inizio ed alla fine della stringa.
//		
//		System.out.println("La lunghezza del nome �: " + nome.trim().length());		//Concatenato due operazioni sulla stringa
//		System.out.println("La lunghezza del nome �: " + (nome.trim()).length());
		
		/* ---------------------------------------------- */

//		String nominativo = "Giovanni Pace";
//		System.out.println(nominativo.toUpperCase());
//		System.out.println(nominativo.toLowerCase());
		
		/* ---------------------------------------------- */

//		String elenco = "Banane, Latte, Biscotti, Lamponi, Cereali";
////		System.out.println(elenco.indexOf("Banane"));
//		
//		if(elenco.indexOf("Lamponi") >= 0) {
//			System.out.println("Ho trovato la stringa alla posizione " + elenco.indexOf("Lamponi"));
//		}
		
		// -------------------------------------------------
		
		String nome = "Giovanni";
		String cognome = new String("Pace");
		System.out.println(cognome);
		
		String matricola;		//matricola = null
		
		cognome = null;
		
		
	}

}
