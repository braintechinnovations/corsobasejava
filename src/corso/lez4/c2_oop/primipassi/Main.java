package corso.lez4.c2_oop.primipassi;

public class Main {

	public static void main(String[] args) {

//		//Firma: Libro()
//		Libro isda = new Libro();
//		isda.setTitolo("Il signore degli anelli");
//		isda.setAutore("Tolkien");
//		isda.setCodice("1234-1234-1234");
//		
//		//Firma: Libro(String, String, String)
//		Libro isdadt = new Libro("Le due torri", "Tolkien", "1234567-789456123");
//		
//		//Firma: Libro(String, String)
//		Libro isdarr = new Libro("Il ritorno del re", "Tolkien");
//		isdarr.setCodice("7489756464651315");
		
		// --------------------------------
		
		Libro libroUno = new Libro("Titolo 1", "Autore 1", "12345");
//		libroUno.stampaLibro();
		
		String valore = libroUno.csvLibro();
		System.out.println(valore);
		
		System.out.println(libroUno.csvLibro());
	}

}
