package corso.lez4.c2_oop.primipassi;

public class Libro {

	private String titolo;
	private String codice;
	private String autore;
	
	public Libro() {
		
	}
	
	/**
	 * Costruttore del Libro con tre parametri
	 * @param varTitolo Titolo del libro da creare
	 * @param varCodice Codice ISBN del libro
	 * @param varAutore Autore del libro
	 */
	public Libro(String varTitolo, String varCodice, String varAutore) {
		this.titolo = varTitolo;
		this.autore = varAutore;
		this.codice = varCodice;
	}
	
	/**
	 * Costruttore con due parametri
	 * @param varTitolo	Titolo del libro da creare
	 * @param varAutore Autore del libro da creare
	 */
	public Libro(String varTitolo, String varAutore) {
		this.titolo = varTitolo;
		this.autore = varAutore;
	}
	
//	public Libro(String varCodice, String varAutore) {		//Esiste un metodo gi� dichiarato con la stessa firma
//		this.codice = varCodice;
//		this.autore = varAutore;
//	}
	
	public void setTitolo(String varTitolo) {
		this.titolo = varTitolo;
	}
	public void setCodice(String varCodice) {
		this.codice = varCodice;
	}
	public void setAutore(String varAutore) {
		this.autore = varAutore;
	}
	
	public String getTitolo() {
		return this.titolo;
	}
	public String getCodice() {
		return this.codice;
	}
	public String getAutore() {
		return this.autore;
	}
	
	/**
	 * Metodo personalizzato per la stampa del dettaglio del libro!
	 */
	public void stampaLibro() {
		String descrizione = "Titolo: " + this.titolo + "\n"
				+ "Autore: " + this.autore + "\n"
				+ "Codice: " + this.codice;
		
		System.out.println(descrizione);
	}
	
	/**
	 * Funzione di composizione dei dettagli libro sotto CSV
	 * @return Versione CSV dell'oggetto Libro
	 */
	public String csvLibro() {
		String csv = this.titolo + ";" + this.autore + ";" + this.codice;
		return csv;
	}
}
