package corso.lez4.c2_oop.utilizzo;

public class Studente {
	
	private String nome;
	private String cognome;
	private String matricola;
	
	private Indirizzo indCasa;
	
	public Studente(){
		
	}
	
	public Studente(String varNome, String varCognome, String varMatricola) {
		this.nome = varNome;
		this.cognome = varCognome;
		this.matricola = varMatricola;
	}
	
	public void setIndCasa(Indirizzo varIndirizzo) {
		this.indCasa = varIndirizzo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getMatricola() {
		return matricola;
	}
	
	//escludo il setMatricola per evitare la modifica della matricola dello studente

	@Override
	public String toString() {
		return "Studente [nome=" + nome + ", cognome=" + cognome + ", matricola=" + matricola + "]";
	}
	
	
	

}
