package corso.lez4.c2_oop.utilizzo;

public class Main {

	public static void main(String[] args) {

		/*
		 * Creare un sistema di gestione studenti che permetta l'inseirmento
		 * di un nuovo studente all'interno di un elenco.
		 * 
		 * Dati: Nome, Cognome, Matricola
		 * 
		 * Primo step:
		 * - Creare una classe Studente
		 * - Creare nel main 3 studenti
		 * - Stampare i dettagli dei 3 studenti
		 */
		
//		Studente gio = new Studente("Giovanni", "Pace", "AB1234");
//		Studente mar = new Studente("Maria", "Rossi", "AB1235");
//		
////		System.out.println(gio);
////		
////		gio = null;
//		
////		System.out.println(gio);
//		
//		gio = new Studente("Giovanni", "Pace", "AB1233");
//	
////		System.out.println(gio.toString());
////		System.out.println(mar);
//		
//		Professore prof = new Professore("Valeria", "Verdi", "Informatica");
//		System.out.println(prof);
		
		// ----------------------------------------
		
		Studente studUno = new Studente("Giovanni", "Pace", "AB1234");
		Indirizzo indStud = new Indirizzo("Via le mani dal naso", "12", "Alba", 00101, "TO");
		studUno.setIndCasa(indStud);
		
		Professore profUno = new Professore("Mario", "Proffi", "Informatica");						//Creo il professore
		Indirizzo indCasa = new Indirizzo("P.zza la bomba e scappa", "SNC", "Roma", 01470, "Roma");	//Creo l'indirizzo prima di passarlo al prof
		profUno.setIndCasa(indCasa);																//Passo l'indirizzo al prof
		Indirizzo indUfficio = new Indirizzo("Via col vento", "20", "Bari", 369852, "BA");			
		profUno.setIndUfficio(indUfficio);
		
		//Versione senza utilizzare una variabile per ogni indirizzo!
		Professore profDue = new Professore("Valeria", "Proffa", "Fisica Nucleare");
		profDue.setIndCasa( new Indirizzo("P.zza la bomba e scappa", "SNC", "Roma", 01470, "Roma") );
		profDue.setIndUfficio( new Indirizzo("Via col vento", "20", "Bari", 369852, "BA") );
		
		System.out.println(profUno.toString());
//		System.out.println(profUno.getIndCasa().toString());
//		System.out.println(profUno.getIndUfficio().toString());
		
//		Indirizzo indUfficioUno = profUno.getIndUfficio();
//		indUfficioUno.setIndirizzo("Parco della vittoria");
//		System.out.println(indUfficioUno.toString());
//		
//		System.out.println(profUno.getIndUfficio().toString());
//		
		
		System.out.println(profUno.toString());
		
	}

}
