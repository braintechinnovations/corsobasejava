package corso.lez4.c2_oop.utilizzo;

public class Professore {

	private String nome;
	private String cognome;
	private String corso;
	
	private Indirizzo indCasa;
	private Indirizzo indUfficio;
	
	public Professore() {
		
	}
	
	public Professore(String varNome, String varCognome, String varCorso) {
		this.nome = varNome;
		this.cognome = varCognome;
		this.corso = varCorso;
	}
	
	public void setIndCasa(Indirizzo varIndCasa) {
		this.indCasa = varIndCasa;
	}
	public void setIndUfficio(Indirizzo varIndUfficio) {
		this.indUfficio = varIndUfficio;
	}
	
	public Indirizzo getIndCasa() {
		return this.indCasa;
	}
	public Indirizzo getIndUfficio() {
		return this.indUfficio;
	}
	

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getCorso() {
		return corso;
	}

	public void setCorso(String corso) {
		this.corso = corso;
	}

	@Override
	public String toString() {
		return "Professore [nome=" + nome + ", cognome=" + cognome + ", corso=" + corso + ", indCasa=" + indCasa
				+ ", indUfficio=" + indUfficio + "]";
	}

	
	
	
}
