package corso.lez4.c2_oop.contenitori.hw;

public class Main {

	public static void main(String[] args) {

		/*
		 * Creare un sistema di gestione di Negozi di fumetti.
		 * Ogni negozio sar� caratterizzato da un nome ed un indirizzo (composto)
		 * 
		 * Ogni fumetto sar� caratterizzato almeno da:
		 * - Titolo
		 * - Anno
		 * - Codice
		 * - Prezzo
		 * - Quantit�
		 * - Categoria (eroi||gothic||fairies) - Stringa che contiene solo uno di questi valori
		 * 
		 * Creare almeno un negozio nel main, inserire almeno 5 fumetti e:
		 * - Stampare tutti i fumetti presenti nel negozio
		 * - Stampare solo i fumetti di una categoria
		 * - HARD: Stampare il valore della merce in negozio tenendo conto delle quantit� e singoli prezzi
		 */
		
		Negozio negGio = new Negozio("Fumetti fighi", "Del vento", "123A", "ROMA", 123456, "Roma");
		negGio.inserisciFumetto("Iron Man", 1976, "IR001", 14.0f, 18, "eroi");
		negGio.inserisciFumetto("Winter Soldie", 1990, "WS001", 14.0f, 19, "eroi");
		negGio.inserisciFumetto("Death Note", 1995, "DN001", 14.0f, 19, "gothic");
		
//		negGio.stampaFumetti();
		negGio.ricercaFumetti("eroi");
		
	}

}
