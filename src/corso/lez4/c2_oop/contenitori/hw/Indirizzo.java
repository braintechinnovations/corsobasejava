package corso.lez4.c2_oop.contenitori.hw;

public class Indirizzo {

	private String via;
	private String civico;
	private String citta;
	private int cap;
	private String provincia;
	
	public Indirizzo() {
		
	}

	public Indirizzo(String via, String civico, String citta, int cap, String provincia) {
		this.via = via;
		this.civico = civico;
		this.citta = citta;
		this.cap = cap;
		this.provincia = provincia;
	}

	public String getVia() {
		return via;
	}

	public void setVia(String indirizzo) {
		this.via = via;
	}

	public String getCivico() {
		return civico;
	}

	public void setCivico(String civico) {
		this.civico = civico;
	}

	public String getCitta() {
		return citta;
	}

	public void setCitta(String citta) {
		this.citta = citta;
	}

	public int getCap() {
		return cap;
	}

	public void setCap(int cap) {
		this.cap = cap;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	@Override
	public String toString() {
		return "Indirizzo [indirizzo=" + via + ", civico=" + civico + ", citta=" + citta + ", cap=" + cap
				+ ", provincia=" + provincia + "]";
	}
	
}
