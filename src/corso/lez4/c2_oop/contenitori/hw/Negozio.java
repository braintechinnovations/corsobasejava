package corso.lez4.c2_oop.contenitori.hw;

import java.util.ArrayList;

public class Negozio {

	private String nome;
	private Indirizzo indirizzo;
	private ArrayList<Fumetto> scaffale = new ArrayList<Fumetto>();
	
	public Negozio(
			String nome, 
			String via, 
			String civico,
			String citta, 
			int cap,
			String provincia) {
		
		this.nome = nome;
		
		Indirizzo indTemp = new Indirizzo(via, civico, citta, cap, provincia);
		this.indirizzo = indTemp;
	}
	
	public void inserisciFumetto(
			String titolo,
			int anno,
			String codice,
			float prezzo,
			int quantita,
			String categoria) {
		
		Fumetto fumTemp = new Fumetto(titolo, anno, codice, prezzo, quantita, categoria);
		scaffale.add(fumTemp);
	}
	
	public void stampaFumetti() {
		for(int i=0; i<this.scaffale.size(); i++) {
			Fumetto temp = this.scaffale.get(i);
			System.out.println(temp.toString());
		}
	}
	
	public void ricercaFumetti(String tipo) {
		for(int i=0; i<this.scaffale.size(); i++) {
			Fumetto temp = this.scaffale.get(i);

			if(temp.getCategoria().equals(tipo)) {
				System.out.println(temp.toString());
			}
		}
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Indirizzo getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(Indirizzo indirizzo) {
		this.indirizzo = indirizzo;
	}
	
	
	
}
