package corso.lez4.c2_oop.contenitori.hw;

public class Fumetto {

	private String 	titolo;
	private int 	anno;
	private String  codice;
	private float  	prezzo;
	private int  	quantita;
	private String  categoria;
	
	public Fumetto() {
		
	}
	
	public Fumetto(String titolo, int anno, String codice, float prezzo, int quantita, String categoria) {
		this.titolo = titolo;
		this.anno = anno;
		this.codice = codice;
		this.prezzo = prezzo;
		this.quantita = quantita;
		this.categoria = categoria;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public int getAnno() {
		return anno;
	}

	public void setAnno(int anno) {
		this.anno = anno;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public float getPrezzo() {
		return prezzo;
	}

	public void setPrezzo(float prezzo) {
		this.prezzo = prezzo;
	}

	public int getQuantita() {
		return quantita;
	}

	public void setQuantita(int quantita) {
		this.quantita = quantita;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	@Override
	public String toString() {
		return "Fumetto [titolo=" + titolo + ", anno=" + anno + ", codice=" + codice + ", prezzo=" + prezzo
				+ ", quantita=" + quantita + ", categoria=" + categoria + "]";
	}
	
	
	
	
}
