package corso.lez4.c2_oop.contenitori.riorganizzazione;

public class Giornale {

	private String titolo;
	private String codice;
	private float prezzo;
	
	public Giornale() {
		
	}

	public Giornale(String titolo, String codice, float prezzo) {
		super();
		this.titolo = titolo;
		this.codice = codice;
		this.prezzo = prezzo;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public float getPrezzo() {
		return prezzo;
	}

	public void setPrezzo(float prezzo) {
		this.prezzo = prezzo;
	}

	@Override
	public String toString() {
		return "Giornale [titolo=" + titolo + ", codice=" + codice + ", prezzo=" + prezzo + "]";
	}
	
	
	
}
