package corso.lez4.c2_oop.contenitori.riorganizzazione;

import java.util.ArrayList;

public class Edicola {

	private String nome;
	private ArrayList<Giornale> elencoGiornali = new ArrayList<Giornale>();
	
	public Edicola(String varNome) {
		this.nome = varNome;
	}
	
	/**
	 * Metodo per la creazione di un nuovo giornale
	 * @param varTitolo
	 * @param varCodice
	 * @param varPrezzo
	 */
	public void creaGiornale(String varTitolo, String varCodice, float varPrezzo) {
		Giornale tempGiornale = new Giornale(varTitolo, varCodice, varPrezzo);
		this.elencoGiornali.add(tempGiornale);
	}
	
	/**
	 * Metodo per la stampa di tutti i giornali
	 */
	public void stampaGiornali() {
		for(int i=0; i<this.elencoGiornali.size(); i++) {
			Giornale temp = this.elencoGiornali.get(i);
			System.out.println(temp.toString());
		}
	}
	
}
