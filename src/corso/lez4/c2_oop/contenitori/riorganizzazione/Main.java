package corso.lez4.c2_oop.contenitori.riorganizzazione;

public class Main {

	public static void main(String[] args) {

		/*
		 * Creare un software gestionale di Edicole.
		 * Ogni edicola avr� al suo interno un elenco di giornali da vendere.
		 */
		
		Edicola centrale = new Edicola("Stazione Centrale");

		centrale.creaGiornale("Il Centro", "123456", 1.5f);
		centrale.creaGiornale("Il messaggero", "98787987", 2.0f);
		
		centrale.stampaGiornali();
		
		System.out.println("------------------------------------");
		
		Edicola periferica = new Edicola("Edicola periferica");
		
		periferica.creaGiornale("Repubblica", "456789", 1.2f);
		periferica.creaGiornale("Il messaggero", "98787987", 2.0f);
		periferica.creaGiornale(null, null, 0);
		
		periferica.stampaGiornali();
	}

}
