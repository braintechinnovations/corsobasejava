package corso.lez4.c2_oop.contenitori;

public class Libro {

	private String titolo;
	private String autore;
	private String codice;
	
	public Libro() {
		
	}

	public Libro(String titolo, String autore, String codice) {
		this.titolo = titolo;
		this.autore = autore;
		this.codice = codice;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public String getAutore() {
		return autore;
	}

	public void setAutore(String autore) {
		this.autore = autore;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	@Override
	public String toString() {
		return "Libro [titolo=" + titolo + ", autore=" + autore + ", codice=" + codice + "]";
	}
	
	
	
	
	
}
