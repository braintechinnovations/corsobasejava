package corso.lez4.c2_oop.contenitori;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {

		Libro libUno = new Libro("Titolo 1", "Autore 1", "111111");
		Libro libDue = new Libro("Titolo 2", "Autore 2", "222222");
		Libro libTre = new Libro("Titolo 3", "Autore 3", "333333");
	
		ArrayList<Libro> elencoLibri = new ArrayList<Libro>();
		elencoLibri.add(libUno);
		elencoLibri.add(libDue);
		elencoLibri.add(libTre);

//		System.out.println(elencoLibri.get(0));			;(
//		System.out.println(elencoLibri.get(1));
//		System.out.println(elencoLibri.get(2));
		
		//Stampa di tutti i libri all'interno dell'elenco
//		for(int i=0; i<elencoLibri.size(); i++) {
//			Libro temp = elencoLibri.get(i);
//			System.out.println(temp.toString());
//			
////			System.out.println(elencoLibri.get(i).toString());			//Versione rapida che non dichiara una variabile
//		}
		
		//Stampa solo tutti i titoli che hai in elenco
//		for(int i=0; i<elencoLibri.size(); i++) {
//			Libro temp = elencoLibri.get(i);
//			System.out.println(temp.getTitolo() + "-" + temp.getCodice());
//		}
		
		//Elimina un libro utilizzando il codice
		String codice = "222222";
		
		for(int i=0; i<elencoLibri.size(); i++) {
			Libro temp = elencoLibri.get(i);
			
			if(temp.getCodice().equals(codice)) {
				elencoLibri.remove(i);
			}
			
		}
		
		for(int i=0; i<elencoLibri.size(); i++) {
			Libro temp = elencoLibri.get(i);
			System.out.println(temp.toString());
		}
		
		
		
		
	}

}
