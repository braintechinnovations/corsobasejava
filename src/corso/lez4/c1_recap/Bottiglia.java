package corso.lez4.c1_recap;

public class Bottiglia {

	private float volume;
	private String materiale;
	private String contenuto;
	
	public Bottiglia() {		//Costruttore
//		System.out.println("Oggetto creato");
	}
	
	public void setContenuto(String varContenuto) {
		if(varContenuto.isBlank()) {	//Controllo che la stringa non sia vuota!
			varContenuto = "N.D.";
		}
		
		this.contenuto = varContenuto;
	}
	public String getContenuto() {
		return this.contenuto;
	}
	
	public void setMateriale(String varMateriale) {
		if(varMateriale.isBlank()) {
			varMateriale = "N.D.";
		}
		
		this.materiale = varMateriale;
	}
	public String getMateriale() {
		return this.materiale;
	}
	
	public void setVolume(float varVolume) {
		if(varVolume < 0) {						//Controllo che il numero non sia negativo
			varVolume = -1 * varVolume;
//			System.out.println("Me stai a prende in giro?");
		}
		
		this.volume = varVolume;
	}
	public float getVolume() {
		return this.volume;
	}
	
	public void stampaBottiglia() {
		String descrizione = this.contenuto + " " 
								+ this.materiale + " " 
								+ this.volume;
		
		System.out.println(descrizione);
	}
}
