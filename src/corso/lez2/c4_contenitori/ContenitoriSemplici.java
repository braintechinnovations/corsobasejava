package corso.lez2.c4_contenitori;

public class ContenitoriSemplici {

	public static void main(String[] args) {

//		String[] elencoAuto = { "BMW", "Lamborghini", "Mercedes", "Bugatti", "Tesla" };
		
//		String[] elencoAuto = new String[5];
//		elencoAuto[0] = "BMW";
//		elencoAuto[1] = "Lamborghini";
//		elencoAuto[2] = "Mercedes";
//		elencoAuto[3] = "Bugatti";
//		elencoAuto[4] = "Tesla";
		
//		System.out.println(elencoAuto[0]);
//		System.out.println(elencoAuto[elencoAuto.length - 1]);
		
//		int i = 0;
//		
//		while(i < elencoAuto.length) {
//			System.out.println(elencoAuto[i]);
//			
//			i++;
//		}
		
		/* -------------------------------------------------------- */
		
//		for(int i=0; i < elencoAuto.length; i++) {
//			System.out.println(elencoAuto[i]);
//		}
		
		/* -------------------------------------------------------- */

//		int[] elencoInteri = { 1,5,2,5,7 };
//		for(int i=0; i<elencoInteri.length; i++) {
//			System.out.println(elencoInteri[i]);
//		}
		
		/* -------------------------------------------------------- */
		
		/*
		 * Dato un array, trovare un modo per verificare se una stringa � presente o meno
		 * al suo interno
		 */
		
		String[] elencoAuto = { "BMW", "Lamborghini", "Mercedes", "Bugatti", "Tesla" };
		String  controlla = "Lamborghini";
		
		for(int i=0; i<elencoAuto.length; i++) {
			if(controlla.equals(elencoAuto[i])) {
				System.out.println("Trovato!");
			}
		}
		
//		boolean trovato = false;
//		for(int i=0; i<elencoAuto.length; i++) {
//			if(controlla.equals(elencoAuto[i])) {
//				trovato = true;
//			}
//		}
//		
//		if(trovato) {
//			System.out.println("Trovato!");
//		}
//		else {
//			System.out.println("Non trovato!");
//		}
		
	}

}
