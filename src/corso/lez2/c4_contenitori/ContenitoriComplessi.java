package corso.lez2.c4_contenitori;

public class ContenitoriComplessi {

	public static void main(String[] args) {

//		String[][] elencoStudenti = {
//				{"Giovanni", "Pace", "AB1234"},
//				{"Mario", "Rossi", "AB1235"},
//				{"Valeria", "Verdi", "AB1236"},
//		};
		
//		System.out.println(elencoStudenti[0][0]);
//		System.out.println(elencoStudenti[0][1]);
//		System.out.println(elencoStudenti[0][2]);
//		System.out.println(elencoStudenti[1][0]);
//		System.out.println(elencoStudenti[1][1]);
//		System.out.println(elencoStudenti[1][2]);
//		System.out.println(elencoStudenti[2][0]);
//		System.out.println(elencoStudenti[2][1]);
//		System.out.println(elencoStudenti[2][2]);
		
//		for(int i=0; i<elencoStudenti.length; i++) {
//			
//			for(int k=0; k<elencoStudenti[i].length; k++) {
//
//				System.out.println(elencoStudenti[i][k]);
//				
//			}
//			
//		}
		
		/* ------------------------------------------------------------------- */
		
		/*
		 * Dato un elenco di interi, calcolare la somma di questi interi:
		 * int[] elencoInteri = { 1,5,2,5,7 };
		 * 
		 * Tip: Dichiarati una variabile intera chiamata Somma ed inizializzala a zero.
		 */
		
//		int[] elencoInteri = { 1,5,2,5,7 };
//		int somma = 0;
//		
//		for(int i=0; i<elencoInteri.length; i++) {
//			somma += elencoInteri[i];
//		}
//		
//		System.out.println(somma);
		
		/* ------------------------------------------------------------------- */

		/*
		 * Dato un elenco di interi, calcolare la somma di questi interi:
		 * int[][] elencoInteri = { 
				{1,3,6,8},
				{1,6},
				{5,7,3}
 			};
		 * 
		 * Tip: Dichiarati una variabile intera chiamata Somma ed inizializzala a zero.
		 */
		
		int[][] elencoInteri = { 
				{1,3,6,8},
				{1,6},
				{5,7,3}
 			};
		
		int somma = 0;
		
		for(int i=0; i<elencoInteri.length; i++) {
			
			for(int k=0; k<elencoInteri[i].length; k++) {
				somma += elencoInteri[i][k];
			}
			
		}
		
		System.out.println(somma);
		
		/* --------------------------------------------------------------- */
		
		/*
		 * Salva il seguente elenco di auto in concessionaria in un array:
		 * BMW - 3
		 * Lamborghini - 2
		 * Maserati - 8
		 * Mercedes - 15
		 * 
		 * Calcolare in maniera automatica il numero di auto totali presenti nella concessionaria
		 */
		
		String[][] elencoConcessionaria = {
				{"BMW", "3"},
				{"Lamborghini", "2"},
				{"Maserati", "8"},
				{"Mercedes", "15"}
		};
		
	}

}
