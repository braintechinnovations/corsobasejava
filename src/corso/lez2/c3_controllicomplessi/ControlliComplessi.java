package corso.lez2.c3_controllicomplessi;

public class ControlliComplessi {

	public static void main(String[] args) {

		/*
		 * Sistema di gestione sigle province
		 * AQ - L'Aquila
		 * MI - Milano
		 * BO - Bologna
		 * PE - Pescara
		 */
		
//		String input = "BO";
//		
//		if(input.equals("AQ")) {
//			System.out.println("L'Aquila");
//		}
//		else {
//			if(input.equals("MI")) {
//				System.out.println("Milano");
//			}
//			else {
//				if(input.equals("BO")) {
//					System.out.println("Bologna");
//				}
//				else {
//					if(input.equals("PE")) {
//						System.out.println("Pescara");
//					}
//				}
//			}
//		}
		
		/* ------------------------------------ */
		
//		String input = "BO";
//		
//		if(input.equals("AQ")) {
//			System.out.println("L'Aquila");
//		}
//		
//		if(input.equals("MI")) {
//			System.out.println("Milano");
//		}
//		
//		if(input.equals("BO")) {
//			System.out.println("Bologna");
//		}
//		
//		if(input.equals("PE")) {
//			System.out.println("Pescara");
//		}
		
		/* ------------------------------------ */

//		String input = "NA";
//		
//		if(input.equals("AQ")) {
//			System.out.println("L'Aquila");
//		} else if(input.equals("MI")) {
//			System.out.println("Milano");
//		} else if(input.equals("BO")) {
//			System.out.println("Bologna");
//		} else if(input.equals("PE")) {
//			System.out.println("Pescara");
//		} else {
//			System.out.println("Non ho trovato la provincia");
//		}
		
		/* ----------------------------------- */
		
		String input = "BO";
		
		switch(input) {
			case "AQ":
				System.out.println("L'Aquila");
				break;
			case "MI":
				System.out.println("Milano");
				break;
			case "BO":
				System.out.println("Bologna");
				break;
			case "PE":
				System.out.println("Pescara");
				break;
			default:
				System.out.println("Non ho trovato la provincia");
				break;
		}
	}
	
}
