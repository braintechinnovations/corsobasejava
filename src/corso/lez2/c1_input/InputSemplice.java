package corso.lez2.c1_input;

import java.util.Scanner;

public class InputSemplice {

	public static void main(String[] args) {

//		Scanner interceptor = new Scanner(System.in);
//		
//		System.out.println("Ciao, dimmi qual'� il tuo nome:");
//		
//		String nome = interceptor.nextLine();
//		System.out.println("Piacere di conoscerti " + nome);
//		
//		interceptor.close();									//Chiudo lo Scanner
		
		// ---------------------------------------------------------------------------
		
//		Scanner interceptor = new Scanner(System.in);
//		
//		System.out.println("Ciao, dimmi qual'� il tuo nome:");
//		
//		String nome = interceptor.nextLine();
//		nome = nome.trim();
//		
//		if(nome.equals("")) {
//			System.out.println("Non hai inserito nessun nome ;(");
//		}
//		else {
//			System.out.println("Piacere di conoscerti " + nome);
//		}
//		
//		interceptor.close();									//Chiudo lo Scanner
		
		// ----------------------------Conversione da stringa a intero -----------------------------

//		String numeroUno = "3";
//		String numeroDue = "4";
//		
//		int numUnoConvertito = Integer.parseInt(numeroUno);
//		int numDueConvertito = Integer.parseInt(numeroDue);
//		
//		System.out.println( numUnoConvertito + numDueConvertito );
		
		// ---------------------------------------------------------------------------
		
//		Scanner interceptor = new Scanner(System.in);
//		
//		System.out.println("Inserisci il tuo nome");
//		String nome = interceptor.nextLine();
//		
//		System.out.println("Inserisci il tuo cognome");
//		String cognome = interceptor.nextLine();
//		
//		System.out.println("Inserisci la tua et�");
//		int eta = Integer.parseInt(interceptor.nextLine());
//		
//		if(eta >= 18) {
//			System.out.println(nome + " " + cognome + " SEI MAGGIORENNE");
//		}
//		else {
//			System.out.println(nome + " " + cognome + " sei minorenne");
//		}
//		
//		interceptor.close();
		
		// ----------------------------------------------------------------------
		
		/**
		 * TODO: Creare un sistema di inserimento anagrafica studente che utilizzi lo Scanner
		 * Le informazioni da memorizzare sono:
		 * - Nome
		 * - Cognome
		 * - Matricola
		 * 
		 * Una volta inserite queste informazioni mi verr� restituita una stringa
		 * composta da: Nome, Cognome - #Matricola
		 * 
		 * Esempio: Giovanni, Pace - #ABCDEFG
		 */
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Inserisci il nome");
		String nome = scan.nextLine();
		
		System.out.println("Inserisci il cognome");
		String cognome = scan.nextLine();
		
		System.out.println("Inserisci la matricola");
		String matricola = scan.nextLine();
		
		String frase = nome.trim() + ", " + cognome.trim() + " - #" + matricola.trim().toUpperCase();
		System.out.println(frase);
		
//		System.out.println(nome.trim() + ", " + cognome.trim() + " - #" + matricola.trim().toUpperCase());
		
		
		
	}	

}
