package corso.lez2.c1_input;

import java.util.Scanner;

public class InputSempliceEsercizio {

	public static void main(String[] args) {

		/**
		 * TODO:
		 * Verificare la possibilitÓ di ingresso all'interno di un locale, verificare che:
		 * 
		 *  La temperatura sia compresa tra 35 e 42 gradi
		 *  La temperatura non superi i 37.5 gradi
		 *  
		 *  Nel caso si possa entrare nel locale, inserire i propri dati:
		 *  - Nome
		 *  - Cognome
		 *  - Numero di telefono
		 *  
		 *  Nel caso in cui la temperatura non rispetti i limiti imposti sopra, 
		 *  vietare l'ingresso con una frase:
		 *  "ERRORE, non puoi entrare!"
		 */
		
//		TIP:
//		String numFloat = "37.5";
//		float numFloatConvertito = Float.parseFloat(numFloat);
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Dimmi la tua temperatura:");
//		float temperatura = Float.parseFloat(scan.nextLine());
		
		String temp = scan.nextLine();
		float temperatura = Float.parseFloat(temp);
		
		if(temperatura > 35.0f && temperatura < 42.0f) {
			
			if(temperatura >= 37.5f) {
				System.out.println("Hai la febbre");
			}
			else {
				System.out.println("Nome:");
				String nome = scan.nextLine();
				
				System.out.println("Cognome:");
				String cognome = scan.nextLine();
				
				System.out.println("Telefono:");
				String telefono = scan.nextLine();
				
				System.out.println("Benvenuto: " + nome + " " + cognome + " " + telefono);
			}
			
		}
		else {
			System.out.println("Errore non puoi entrare!");
		}
		
		
		
		
		
	}

}
