package corso.lez2.c2_ciclisemplici;

public class CicliSemplici {

	public static void main(String[] args) {

		/*
		 * while(condizione){
		 * 		//Blocco di codice
		 * }
		 * 
		 * INDICE - CONDIZIONE - INCREMENTO
		 */
		
//		int indice = 0;
//		int valMassimo = 0;
//		
//		while(indice < valMassimo) {											//Verifica a priori della condizione
//			System.out.println("Valore dell'indice: " + indice);
//			
//			indice = indice + 1;
//		}
		
		// ------------------------------------------------
		
		/*
		 * do{
		 * 		//Blocco di codice
		 * } while(condizione)
		 */
		
//		int indice = 0;
//		int valMassimo = 0;
//		
//		do {
//			System.out.println("Valore dell'indice: " + indice);
//			
//			indice = indice + 1;
//		} while(indice < valMassimo);										//Verifica a posteriori della condizione
	}

}
