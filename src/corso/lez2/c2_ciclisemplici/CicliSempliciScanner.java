package corso.lez2.c2_ciclisemplici;

import java.util.Scanner;

public class CicliSempliciScanner {

	public static void main(String[] args) {
		
//		Scanner scan = new Scanner(System.in);
//		
//		int indice = 0;
//		int valMax = 3;
//		
//		while(indice < valMax) {
//			System.out.println("Inserisci la persona " + (indice + 1));	//Incremento il valore solo a livello grafico
//			
//			String nome = scan.nextLine();								//Variabile locale
//			System.out.println("Ciao " + nome);
//			
//			indice++; 				//indice = indice + 1;
//		}
//		
////		System.out.println(nome);		//Errore, la variabile nome fa parte del contesto While e non ci posso accedere!
		
		/* ------------------------------------------------------------------------------ */
		
//		Scanner scan = new Scanner(System.in);
//		
//		int indice = 0;
//		int valMax = 3;
//		
//		String nome = "";
//		
//		while(indice < valMax) {
//			System.out.println("Inserisci la persona " + (indice + 1));	//Incremento il valore solo a livello grafico
//			
//			nome = scan.nextLine();
//			System.out.println("Ciao " + nome);
//			
//			indice++; 					//indice = indice + 1;
//		}
//		
//		System.out.println(nome);		//Attenzione, il valore in output � l'ultimo salvato!
		
		/* ----------------------------------------------------------------------------- */
		
//		int valore = 0;
//		valore = valore + 1;			//1
//		valore = valore + 1;			//2
//		
//		String frase = "";
//		frase = frase + "Giovanni";		//Giovanni
//		frase = frase + " Pace"; 		//Giovanni Pace
		
		/* ----------------------------------------------------------------------------- */

//		Scanner scan = new Scanner(System.in);
//		
//		int i = 0;
//		int max = 3;
//		
//		String rubrica = "";
//		
//		while(i < max) {
//			System.out.println("Come si chiama il contatto?");
//			
//			String nome = scan.nextLine();
//			rubrica = rubrica + nome + "\n";
//			
//			i++;
//		}
//		
//		System.out.println(rubrica);
		
		/* ----------------------------------------------------------------------------- */

//		Scanner scan = new Scanner(System.in);
//		
//		boolean scritturaAbilitata = true;
//		
//		while(scritturaAbilitata) {
//			
//			System.out.println("Inserisci il nome oppure digita QUIT per uscire");
//			
//			String input = scan.nextLine();
//			if(input.equals("QUIT")) {
//				scritturaAbilitata = false;
//			}
//			else {
//				System.out.println("Ciao " + input);
//			}
//			
//		}
//		
//		scan.close();
		
		/* ----------------------------------------------------------------------------- */

		//Negazione di una variabile booleana per ottenere il suo opposto
//		boolean variabileBooleana = true;
//		System.out.println(!variabileBooleana);
		
		/* ----------------------------------------------------------------------------- */
		
		/*
		 * Inserisci un numero indefinito di persone in rubrica finch� non digito la parola QUIT
		 * Alla fine dell'inserimento mandare in output tutti i nomi inseriti!
		 */
		
//		Scanner scan = new Scanner(System.in);
//		
//		String rubrica = "";
//		boolean scritturaAbilitata = true;
//		
//		while(scritturaAbilitata) {
//			
//			System.out.println("Inserisci il nome o digita QUIT:");
//			String input = scan.nextLine();
//			
//			if(!input.equals("QUIT")) {
//				rubrica += input + "\n";         // rubrica = rubrica + input + "\n";
//			}
//			else {
//				scritturaAbilitata = false;
//			}
//		}
//		
//		System.out.println("----------------------");
//		System.out.println(rubrica);
		
		/* --------------------------------------------------------------------------- */
		
		/*
		 * Creare un sistema di input che abbia come finalit� la creazione di un elenco di studenti.
		 * Uno studente � caratterizzato da NOME, COGNOME, MATRICOLA (tutto alfanumerico)
		 * Ad ogni inserimento, questi studenti verranno elencati sequenzialmente e stampati in Console.
		 * 
		 * TIP: Create una stringa chiamata Lista Studenti!
		 * 
		 * Giovanni, Pace, #12345
		 * Mario, Rossi, #12346
		 */
		
//		Scanner scan = new Scanner(System.in);
//		
//		String elenco = "";
//		boolean inserimentoConsentito = true;
//		
//		while(inserimentoConsentito) {
//			
//			System.out.println("Nome (o QUIT):");
//			String nome = scan.nextLine();
//			
//			if(nome.equals("QUIT")) {
//				inserimentoConsentito = false;
//			}
//			else {
//				
//				System.out.println("Cognome (o QUIT):");
//				String cognome = scan.nextLine();
//				
//				if(cognome.equals("QUIT")) {
//					inserimentoConsentito = false;
//				}
//				else {
//					
//					System.out.println("Matricola (o QUIT):");
//					String matricola = scan.nextLine();
//					
//					if(matricola.equals("QUIT")) {
//						inserimentoConsentito = false;
//					}
//					else {
//						elenco += nome + ", " + cognome + ", " + matricola + "\n";
//						System.out.println(elenco);
//					}
//					
//				}
//				
//			}
//			
//		}
		
		//Propeutico corso.lez2.c2_controllicomplessi
		/* --------------------------------------------------------------- */
		
//		Scanner scan = new Scanner(System.in);
//		
//		String elenco = "";
//		boolean inserimentoAbilitato = true;
//		
//		while(inserimentoAbilitato) {
//			
//			System.out.println("Seleziona la funzione che ti aggrada:\n"
//					+ "I - Inserimento\n"
//					+ "S - Stampa\n"
//					+ "Q - Uscita");
//			
//			String input = scan.nextLine();
//			
//			switch(input) {
//				case "I":
//					System.out.println("Dammi il nome:");
//					String nome = scan.nextLine();
//					
//					elenco += nome + "\n";
//					break;
//				case "S":
//					System.out.println(elenco);
//					break;
//				case "Q":
//					inserimentoAbilitato = false;
//					break;
//				default:
//					System.out.println("Comando non riconosciuto");
//			}
//			
//		}
		
	}
	
}
