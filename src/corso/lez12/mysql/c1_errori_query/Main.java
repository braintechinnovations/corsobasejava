package corso.lez12.mysql.c1_errori_query;

import java.sql.SQLException;

public class Main {

	public static void main(String[] args) {
		
		GestionePersona gestore = new GestionePersona();
		
		Persona perUno = new Persona();
		perUno.setPer_nome("Marika");
		perUno.setPer_cogn("Akiram");
		perUno.setPer_tele("789456");
		perUno.setPer_codf("MARAKI");
		
		try {
			gestore.inserisciPersona(perUno);
			System.out.println(perUno);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		try {
			Persona perDue = gestore.cercaPerId(7);
			System.out.println(perDue);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
	}
	
}
