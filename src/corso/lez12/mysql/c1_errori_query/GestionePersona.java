package corso.lez12.mysql.c1_errori_query;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class GestionePersona {

private MysqlDataSource dataSource = new MysqlDataSource();
	
	public GestionePersona() {
		this.dataSource.setServerName("localhost");	//Oppure 127.0.0.1
		this.dataSource.setPortNumber(3306);
		this.dataSource.setUser("root");
		this.dataSource.setPassword("toor");
		this.dataSource.setDatabaseName("rubrica");
		this.dataSource.setUseSSL(false);	
	}

	/**
	 * Funzione di inserimento della persona
	 * @param objPersona
	 * @throws SQLException potrebbe restituire una SQLException
	 */
	public void inserisciPersona(Persona objPersona) throws SQLException {
		Connection conn = (Connection) this.dataSource.getConnection();
	
		String queryInserimento = "INSERT INTO persona "
				+ "(nome, cognome, telefono, codice_fiscale) VALUES (?,?,?,?)"; 
		
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(queryInserimento, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, objPersona.getPer_nome());	
		ps.setString(2, objPersona.getPer_cogn());
		ps.setString(3, objPersona.getPer_tele());
		ps.setString(4, objPersona.getPer_codf());
		
		ps.executeUpdate();
		ResultSet rs = ps.getGeneratedKeys();
		
		rs.next();
		int idGenerato = rs.getInt(1);
		objPersona.setPer_id(idGenerato);
	}
	
	/**
	 * Restituisce un ArrayList con tutte le persone trovate dalla select
	 * @return ArrayList<Persona>
	 * @throws SQLException 
	 */
	public ArrayList<Persona> cercaTuttePersone() throws SQLException {
		
		ArrayList<Persona> elenco = new ArrayList<Persona>();
		
		Connection conn = (Connection) this.dataSource.getConnection();
		
		String queryRicerca = "SELECT personaId, nome, cognome, "
				+ "telefono, codice_fiscale FROM persona";
		
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(queryRicerca);
		
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			Persona temp = new Persona();
			temp.setPer_id(rs.getInt(1));
			temp.setPer_nome(rs.getString(2));
			temp.setPer_cogn(rs.getString(3));
			temp.setPer_tele(rs.getString(4));
			temp.setPer_codf(rs.getString(5));
			
			elenco.add(temp);
		}
		
		return elenco;
		
	}
	
	/**
	 * Ricerco la persona per ID direttamente fornito al metodo
	 * @param perId	intero identificativo della persona sul DB (campo personaId)
	 * @return	Persona di riferimento | null
	 * @throws SQLException 
	 */
	public Persona cercaPerId(int perId) throws SQLException {
		
			Connection conn = (Connection) this.dataSource.getConnection();
			
			String queryRicerca = "SELECT personaId, nome, cognome, "
					+ "telefono, codice_fiscale FROM persona WHERE personaId = ?";
			
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(queryRicerca);
			ps.setInt(1, perId);
			
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {						//Se ci sono 0 risultati, rs.next() sar� FALSE
				Persona temp = new Persona();
				temp.setPer_id(rs.getInt(1));
				temp.setPer_nome(rs.getString(2));
				temp.setPer_cogn(rs.getString(3));
				temp.setPer_tele(rs.getString(4));
				temp.setPer_codf(rs.getString(5));
				
				return temp;
			}
			
			return null;
		
	}
	
	public boolean eliminaPersona(Persona objPersona) throws SQLException {
		
		Connection conn = (Connection) this.dataSource.getConnection();
		
		String queryEliminazione = "DELETE FROM persona WHERE personaId = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(queryEliminazione);
		ps.setInt(1, objPersona.getPer_id());

		int affRows = ps.executeUpdate();
		if(affRows > 0) {
			return true;
		}
		
		return false;
		
	}
	
	public boolean eliminaPersona(int idPersona) throws SQLException {
		
		Connection conn = (Connection) this.dataSource.getConnection();
		
		String queryEliminazione = "DELETE FROM persona WHERE personaId = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(queryEliminazione);
		ps.setInt(1, idPersona);

		int affRows = ps.executeUpdate();
		if(affRows > 0) {
			return true;
		}
		
		return false;
		
	}
	
	public boolean modificaPersona(Persona objPersona) throws SQLException {
			Connection conn = (Connection) this.dataSource.getConnection();
			
			String queryModifica = "UPDATE persona SET "
													+ "nome = ?, "
													+ "cognome = ?, "
													+ "telefono = ?, "
													+ "codice_fiscale = ? "
													+ "WHERE personaId = ?";
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(queryModifica);
			ps.setString(1, objPersona.getPer_nome());
			ps.setString(2, objPersona.getPer_cogn());
			ps.setString(3, objPersona.getPer_tele());
			ps.setString(4, objPersona.getPer_codf());
			ps.setInt(5, objPersona.getPer_id());
			
			int affRows = ps.executeUpdate();
			if(affRows > 0)
				return true;			//Una sola condizione nella if SENZA le { }
			
			return false;
	}
	
	public void stampaRisultati(ArrayList<Persona> varElenco) {
		
		for(int i=0; i<varElenco.size(); i++) {
			Persona temp = varElenco.get(i);
			System.out.println(temp.toString());
		}
		
	}
	
	//DA VEDERE SINGLETON
	
}
