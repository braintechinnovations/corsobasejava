CREATE DATABASE negozio;
USE negozio;

-- Modalit� compatta di scrittura dei VINCOLI
/*
CREATE TABLE cartafedelta (
	codice_carta VARCHAR(10) NOT NULL PRIMARY KEY,
    nominativo VARCHAR(250) NOT NULL,
    eta INTEGER,
    codice_fiscale VARCHAR(16) NOT NULL UNIQUE,
);
*/

DROP TABLE cartafedelta;

CREATE TABLE cartafedelta (
	codice_carta VARCHAR(10) NOT NULL,		-- Definisco un campo che non pu� essere vuoto!
    nominativo VARCHAR(250) NOT NULL,
    eta INTEGER CHECK (eta >= 18),			-- Verifica che il campo et� sia maggiore o uguale di 18
    codice_fiscale VARCHAR(16) NOT NULL,
    PRIMARY KEY (codice_carta),
    UNIQUE(codice_fiscale)
);

-- DML: Linguaggio di manipolazione dei dati
INSERT INTO cartafedelta(codice_carta, nominativo, eta, codice_fiscale) VALUES
("123456", "Giovanni Pace", 19, "PCAGNN");

INSERT INTO cartafedelta(codice_carta, nominativo, eta, codice_fiscale) VALUES
("123457", "MARIO ROSSI", 20, "MRRRSS");

INSERT INTO cartafedelta(codice_carta, nominativo, eta, codice_fiscale) VALUES
("123460", "Giovanni Pace", 19, "PCAGNN");			-- Non possibile perch� il codice fiscale � duplicato e viola il campo definito UNIQUE (codice_fiscale)

INSERT INTO cartafedelta(codice_carta, nominativo, eta, codice_fiscale) VALUES
("123461", "Valeria Verdi", 12, "VLRVRD");			-- Non possibile perch� il campo eta viola il constraint del eta >= 18

-- QL
SELECT * FROM cartafedelta;


