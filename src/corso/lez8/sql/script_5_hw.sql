-- Tnx to Cristian ;)
DROP DATABASE IF EXISTS musei;
CREATE DATABASE musei;
USE musei;

CREATE TABLE museo (
museoId INTEGER NOT NULL AUTO_INCREMENT,
nome VARCHAR (50),
citta VARCHAR (50),
PRIMARY KEY (museoID)
);

CREATE TABLE opera (
operaId INTEGER NOT NULL AUTO_INCREMENT,
titolo VARCHAR(50),
annoCreazione VARCHAR(10),
PRIMARY KEY (operaId)
);

CREATE TABLE artista (
artistaId INTEGER NOT NULL AUTO_INCREMENT,
nome VARCHAR(50),
PRIMARY KEY (artistaId)
);

CREATE TABLE museo_opera (
MuseoIdRif INTEGER NOT NULL,
OperaIdRif INTEGER NOT NULL,
FOREIGN KEY (MuseoIdRif) REFERENCES museo(museoId),
FOREIGN KEY (OperaIdRif) REFERENCES opera(operaId),
UNIQUE (MuseoIdRif, OperaIdRif)
);

CREATE TABLE opera_artista (
OperaIdRif INTEGER NOT NULL,
ArtistaIdRif INTEGER NOT NULL,
FOREIGN KEY (OperaIdRif) REFERENCES opera(operaId),
FOREIGN KEY (ArtistaIdRif) REFERENCES artista(artistaId),
UNIQUE (OperaIdRif, ArtistaIdRif)
);

INSERT INTO museo (nome, citta) VALUES
("Musei Vaticani", "Roma"),
("Galleria degli Uffizi", "Firenze");

INSERT INTO opera (titolo, annoCreazione) VALUES
("Trasfigurazione", "1520"),
("La nascita di Venere", "1445"),
("Cappella Sistina", "1512"),
("Sacra famiglia", "1475"),
("Deposizione", "1603"),
("Annunciazione", "1470"),
("Scuola di Atene", "1475"),
("La primavera", "1445");

INSERT INTO museo_opera(MuseoIdRif , OperaIdRif) VALUES
(1, 1),
(2, 2),
(1, 3),
(2, 4),
(1, 5),
(2, 6),
(1, 7),
(2, 8);

INSERT INTO artista (nome) VALUES
("Raffaelo Sanzio"),
("Sandro Botticelli"),
("Michelangelo Buonarroti"),
("Caravaggio"),
("Leonardo Da Vinci");

INSERT INTO opera_artista(OperaIdRif, ArtistaIdRif) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 3),
(5, 4),
(6, 5),
(7, 1),
(8, 2);

-- select * from artista;

-- Tutte le opere presenti in tutti i musei
SELECT * FROM museo
JOIN museo_opera ON museo.museoId = museo_opera.MuseoIdRif
JOIN opera ON museo_opera.OperaIdRif = opera.OperaId ;

-- Tutte le opere presenti nei musei vaticani
SELECT titolo
FROM opera
JOIN museo_opera ON opera.operaId = museo_opera.OperaIdRif
JOIN museo ON museo_opera.MuseoIdRif = museo.MuseoId
WHERE nome = "Musei Vaticani";

-- Tutte le opere fatte dall'artista Caravaggio
SELECT * FROM opera
JOIN opera_artista ON opera.operaId = opera_artista.OperaIdRif
JOIN artista ON opera_artista.ArtistaIdRif = artista.ArtistaId
WHERE nome = "Caravaggio";

-- Tutti gli artisti presenti in un museo
SELECT artista.nome
FROM museo
JOIN museo_opera on museo.museoId = museo_opera.MuseoIdRif
JOIN opera on museo_opera.OperaIdRif = opera.operaId
JOIN opera_artista on opera.operaId = opera_artista.OperaIdRif
JOIN artista on opera_artista.ArtistaIdRif = artista.artistaId
WHERE museo.nome = "Musei Vaticani";