/*
Creare il database in grado di gestire le richieste di un E-Commerce
Andremo a tracciare le informazioni relative ai Prodotti, caratterizzati da un codice univoco,
un nome, un prezzo e le categorie di appartenenza.
Ogni categoria sar� caratterizzata da un nome ed un codice categoria.
Inoltre, voglio tracciare le informazioni degli utenti che si iscrivono nel mio sistema e che
effettuano un ordine. Di ogni ordine voglio un codice identificativo e la data in cui � stato
effettuato (in automatico).

1. Inserite 3 prodotti, tre utenti e sei ordini.
2. Cercare tutti gli ordini di un utente
3. Cercare tutti i prodotti afferenti ad una singola categoria
4. Mandare in output tutti i prodotti acquistati da un utente
(presenti all'interno di tutti gli ordini)
5. Mandare in output tutte le categorie di prodotto acquistate da un utente

STEP IMPLEMENTATIVI:
1- Diagramma ER <- :)
2- Scrittura SQL
3- Reverse Engineer (per capire subito come fare le query)
*/