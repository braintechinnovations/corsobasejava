-- CREATE DATABASE test;				-- Creazione di un nuovo Database Ctrl + Invio esegue l'esecuzione dov'� il cursore
-- DROP DATABASE test;					-- Eliminazione di un intero database

-- CREATE DATABASE Rubrica;

USE Rubrica;							-- Seleziona il database sul quale operer� nelle successive operazioni
-- DDL

CREATE TABLE elenco (
	nome VARCHAR(250),
    cognome VARCHAR(250),
    telefono VARCHAR(25),
    codice_fiscale VARCHAR(16),
    PRIMARY KEY(codice_fiscale)
);

-- DROP TABLE elenco;					-- Eliminare una tabella con tutti i suoi dati!!!!!!!!!!

-- DDL per modificare la tabella
-- ALTER TABLE elenco ADD COLUMN codice_fiscale VARCHAR(16);	-- Aggiunta di una colonna
-- ALTER TABLE elenco DROP COLUMN codice_fiscale;			-- ELiminazione di una colonna
-- ALTER TABLE elenco MODIFY COLUMN codice_fiscale CHAR(16);	-- Modifica del tipo di dato ospitabile da una colonna!

