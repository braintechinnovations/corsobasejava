DROP DATABASE IF EXISTS studente_esame;
CREATE DATABASE studente_esame;
USE studente_esame;

CREATE TABLE studente(
	StudenteID INTEGER NOT NULL AUTO_INCREMENT,
    nominativo VARCHAR(250),
    matricola VARCHAR(20) UNIQUE,
    PRIMARY KEY (StudenteID)
);

CREATE TABLE esame(
	EsameID INTEGER NOT NULL AUTO_INCREMENT,
    nome_esame VARCHAR(250) NOT NULL,
    crediti INTEGER DEFAULT 1,					-- Valore di default che verr� popolato solo se nella insert non specifico il campo
    data_esame DATETIME NOT NULL,					-- Si utilizza come una stringa con sintastti YYYY-MM-DD
	PRIMARY KEY(EsameID)
);

CREATE TABLE studente_esame(
	StudenteIdRif INTEGER NOT NULL,
	EsameIdRif INTEGER NOT NULL,
    data_iscrizione DATETIME DEFAULT NOW(),		-- Data di default
    FOREIGN KEY (StudenteIdRif) REFERENCES Studente(StudenteID) ON DELETE CASCADE,
    FOREIGN KEY (EsameIdRif) REFERENCES Esame(EsameID) ON DELETE CASCADE,
    UNIQUE (StudenteIdRif, EsameIdRif)			-- Evito che possa inserire pi� di una volta la combinazione StudenteEsame e quindi iscrivere pi� volte uno studente ad un esame!
);

INSERT INTO studente(nominativo, matricola) VALUES
("Giovanni Pace", "AB-12345"),
("Mario Rossi", "AB-12346"),
("Valeria Verdi", "AB-12347"),
("Maria Viola", "AB-12348");

INSERT INTO esame(nome_esame, crediti, data_esame) VALUES
("Fisica", 6, "2021-01-02 09:10:00"),
("Chimica", 6, "2021-01-02 09:10:00"),
("Meccanica", 6, "2021-01-02 09:10:00"),
("Analisi", 6, "2021-01-02 09:10:00");

INSERT INTO studente_esame(StudenteIdRif, EsameIdRif) VALUES
(1,	4),
(1,	1),
-- (1,	4), -- Duplicato non permesso grazie alla UNIQUE
(3,	2),
(3,	3);

-- SELECT * FROM studente;
-- SELECT * FROM studente_esame;
-- SELECT * FROM esame;

SELECT *
	FROM studente
    JOIN studente_esame ON studente.StudenteID = studente_esame.StudenteIdRif	-- Salto 1
    JOIN esame ON studente_esame.EsameIdRif = esame.EsameID
    WHERE nominativo = "Giovanni Pace";

SELECT * 
	FROM esame
    JOIN studente_esame ON esame.EsameID = studente_esame.EsameIdRif
    JOIN studente ON studente_esame.StudenteIdRif = studente.StudenteID;
    -- WHERE nome_esame = "Analisi"			-- Proietto tutte le possibili combinazioni
    
-- DELETE FROM studente WHERE StudenteID = 1;	-- Possibile solo grazie al On Delete Cascade
-- SELECT * FROM esame;

-- DDL: Definisce lo schema
-- DML: Manipola i dati
-- QL: Estrae informazioni senza manipolare dati!
