/*
Obiettivo: Creare due strutture separate per immagazinare le informazioni relative a:
- Persona
- Carte fedelt�
*/
DROP DATABASE IF EXISTS negozio_carte;		-- Elimina il database se esiste, con tutte le sue tabelle
CREATE DATABASE negozio_carte;
USE negozio_carte;

CREATE TABLE persona(
	PersonaID INTEGER NOT NULL AUTO_INCREMENT,		-- Incrementa in autonomia il campo PersonaID
	nome VARCHAR(250) NOT NULL,
    cognome VARCHAR(250) NOT NULL,
    telefono VARCHAR(20) NOT NULL,
    indirizzo VARCHAR(250) NOT NULL,
    PRIMARY KEY(PersonaID)
);

-- Quando inserisco A MANO un PersonaID, devo utilizzare INSERT INTO .. VALUE (senza S)
INSERT INTO persona(PersonaID, nome, cognome, telefono, indirizzo) VALUE
(34, "Maria", "Viola", "12348", "Via test 4");

INSERT INTO persona(nome, cognome, telefono, indirizzo) VALUES
("Giovanni", "Pace", "12345", "Via test 1"),
("Mario", "Rossi", "12346", "Via test 2"),
("Valeria", "Verdi", "12347", "Via test 1");


-- DML: Linguaggio di Manipolazione dei dati
-- DELETE FROM persona WHERE PersonaID = 35;		-- Elimina un oggetto tramite ID

-- Alias non pu� essere utilizzato nelle clausule Where
-- SELECT cognome, indirizzo AS casa, nome FROM persona WHERE indirizzo = "Via test 1";

-- SELECT PersonaID 									-- Seleziona un solo campo
-- 		FROM persona 
--    	WHERE nome = "Giovanni" AND cognome = "Pace";
    
CREATE TABLE carta(
	CartaID INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	codice VARCHAR(20) NOT NULL UNIQUE,
    negozio VARCHAR(250) NOT NULL,
    PersonaRif INTEGER,
    FOREIGN KEY (PersonaRif) REFERENCES persona(PersonaID) ON DELETE CASCADE
);

INSERT INTO carta (codice, negozio) VALUES
("AB123456", "Coop"),
("AB123457", "Coop"),
("AB123458", "Conad");

UPDATE carta SET PersonaRif = 35 WHERE CartaID = 1;
UPDATE carta SET PersonaRif = 36 WHERE CartaID = 2;
UPDATE carta SET PersonaRif = 35 WHERE CartaID = 3;

DELETE FROM persona WHERE PersonaID = 35;

SELECT * FROM persona;
SELECT * FROM carta;
