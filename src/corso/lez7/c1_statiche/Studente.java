package corso.lez7.c1_statiche;

public class Studente {

	private String nominativo;
	private String martricola;
	
	public static int contatore = 0;
	
	public Studente() {
		contatore++;					//contatore = contatore + 1;
	}
	
	public Studente(String nominativo, String martricola) {
		this.nominativo = nominativo;
		this.martricola = martricola;
		contatore++;
	}

	public String getNominativo() {
		return nominativo;
	}

	public void setNominativo(String nominativo) {
		this.nominativo = nominativo;
	}

	public String getMartricola() {
		return martricola;
	}

	public void setMartricola(String martricola) {
		this.martricola = martricola;
	}
	
	
	
}
