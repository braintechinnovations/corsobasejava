package corso.lez7.c1_statiche.espansione;

public class Docente {
	
	private String nominativo;
	private String dipartimentiAfferenza;
	
	private static int contatore = 0;
	
	public Docente() {
//		Contatore.contatoreGlobale++;
		Contatore.incrementaContatore();
		contatore++;
	}
	
	public static int getContatore() {
		return contatore;
	}
	

	public String getNominativo() {
		return nominativo;
	}

	public void setNominativo(String nominativo) {
		this.nominativo = nominativo;
	}

	public String getDipartimentiAfferenza() {
		return dipartimentiAfferenza;
	}

	public void setDipartimentiAfferenza(String dipartimentiAfferenza) {
		this.dipartimentiAfferenza = dipartimentiAfferenza;
	}
	
}
