package corso.lez7.c1_statiche.espansione;

public class Contatore {
	
	private static int contatoreGlobale = 0;
	
	public static void incrementaContatore() {
		contatoreGlobale++;
	}
	
	public static int getContatoreGlobale() {
		return contatoreGlobale;
	}
}
