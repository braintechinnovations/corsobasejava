package corso.lez7.c1_statiche.espansione;

public class Studente {

	private String nominativo;
	private String matricola;
	
	private static int contatore = 0;
	
	public Studente() {
//		Contatore.contatoreGlobale++;
		Contatore.incrementaContatore();
		contatore++;
	}
	
	public static int getContatore() {
		return contatore;
	}

	public String getNominativo() {
		return nominativo;
	}

	public void setNominativo(String nominativo) {
		this.nominativo = nominativo;
	}

	public String getMatricola() {
		return matricola;
	}

	public void setMatricola(String matricola) {
		this.matricola = matricola;
	}
	
	
	
}
