package corso.lez7.c1_statiche.espansione;

public class Main {

	public static void main(String[] args) {

		Docente docUno = new Docente();
		Docente docDue = new Docente();
		Studente studUno = new Studente();
		
		System.out.println(Contatore.getContatoreGlobale());
		
		System.out.println("Docenti: " + Docente.getContatore());
		System.out.println("Studenti: " + Studente.getContatore());
	}

}
