package corso.lez7.c1_statiche;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

//		Studente gio = new Studente("Giovanni", "AB1234");
//		Studente mar = new Studente();
//		Studente val = new Studente("Valeria", "AB1236");
//		
//		System.out.println(Studente.contatore);
		
		Scanner scan = new Scanner(System.in);
		
		boolean inserimentoAbilitato = true;
		while(inserimentoAbilitato) {
			
			System.out.println("Cosa vuoi fare?\n"
					+ "I - Inserisci\n"
					+ "C - Conta gli studenti inseriti\n"
					+ "Q - Esci\n");
			
			String input = scan.nextLine();
			switch (input) {
				case "I":
					Studente temp = new Studente();
					
					System.out.println("Nominativo:");
					temp.setNominativo(scan.nextLine());		//String nome = scan.nextLine(); temp.setNominativo(nome);
					
					System.out.println("Matricola:");
					temp.setMartricola(scan.nextLine());		//String matricola = scan.nextLine(); temp.setMartricola(matricola);
					break;
				case "C":
					System.out.println("Il numero di studenti �: " + Studente.contatore);
					break;
				case "Q":
					inserimentoAbilitato = !inserimentoAbilitato;		//Togglare
					break;
	
				default:
					break;
			}
		}
	}

}
