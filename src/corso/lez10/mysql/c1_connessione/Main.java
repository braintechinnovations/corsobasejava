package corso.lez10.mysql.c1_connessione;

import java.sql.ResultSet;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class Main {

	public static void main(String[] args) {
		
		//Come se definissi un oggetto con le impostazioni di connessione
		MysqlDataSource dataSource = new MysqlDataSource();
		dataSource.setServerName("localhost");	//Oppure 127.0.0.1
		dataSource.setPortNumber(3306);
		dataSource.setUser("root");
		dataSource.setPassword("toor");
		dataSource.setDatabaseName("rubrica");
		dataSource.setUseSSL(false);				//Dalla 8.0 in poi di MySql Server
		
		try {
			Connection conn = (Connection) dataSource.getConnection();
			System.out.println("Sei connesso");
			
			//INSERT
			
//			String nome = "Valeria";
//			String cognome = "Pace";
//			String telefono = "123457";
//			String codice_fiscale = "PCAVLR";
//			
//			String query_inserimento = "INSERT INTO persona (nome, cognome, telefono, codice_fiscale) VALUES (?,?,?,?)";
//			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query_inserimento, Statement.RETURN_GENERATED_KEYS);
//			
//			ps.setString(1, nome);
//			ps.setString(2, cognome);
//			ps.setString(3, telefono);
//			ps.setString(4, codice_fiscale);
//			
//			ps.executeUpdate();						//Esecuzione del prepared statement sul DBMS, qui inserisco il dato
//			
//			//Da qui in poi serve per prendere il valore Auto Increment generato
//			ResultSet rs = ps.getGeneratedKeys();	//Salvo il dato ritornato grazie al Statement.RETURN_GENERATED_KEYS all'interno di un ResultSet
//			rs.next();								//Entro nello scatolone Result Set
//			
//			int chiaveGenerata = rs.getInt(1);		//Dato che il result set � composto da una riga ed una colonna, posso accedere direttamente alla colonna 1 di tipo INT
//			if(chiaveGenerata > 0) {				//Se la chiave primaria � maggiore di zero, allora OK! ;)
//				System.out.println("Ho effettuato l'inserimento correttamente!");
//			}
//			else {
//				System.out.println("Errore nell'inserimento della riga");
//			}
			
			//SELECT all
			
//			String querySelect = "SELECT personaId, nome, cognome, telefono, codice_fiscale FROM persona";
//			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(querySelect);
//			
//			ResultSet rs = ps.executeQuery();
//			
//			while(rs.next()) {
//				int personaId = rs.getInt(1);
//				String nome = rs.getString(2);
//				String cognome = rs.getString(3);
//				String telefono = rs.getString(4);
//				String cf = rs.getString(5);
//				
//				System.out.println(personaId + ", " + nome + ", " + cognome + ", " + telefono + ", " + cf);
//			}
			
			//Seleziona persone tramite ID?
			
//			int idSelezionato = 3;
//			
//			String querySelect = "SELECT personaId, nome, cognome, telefono, codice_fiscale "
//					+ "FROM persona WHERE personaId = ?";
//			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(querySelect);
//			ps.setInt(1, idSelezionato);
//			
//			ResultSet rs = ps.executeQuery();
//			
//			while(rs.next()) {
//				int personaId = rs.getInt(1);
//				String nome = rs.getString(2);
//				String cognome = rs.getString(3);
//				String telefono = rs.getString(4);
//				String cf = rs.getString(5);
//				
//				System.out.println(personaId + ", " + nome + ", " + cognome + ", " + telefono + ", " + cf);
//			}
			
			//DELETE 
			
//			int idDaEliminare = 3;
//			
//			String queryDelete = "DELETE FROM persona WHERE personaId = ?";
//			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(queryDelete);
//			ps.setInt(1, idDaEliminare);
//			
//			int affectedRows = ps.executeUpdate();
//			if(affectedRows > 0) {
//				System.out.println("Operazione effettuata correttamente");
//			}
//			else {
//				System.out.println("Errore nell'esecuzione dell'operazione");
//			}
			
			//UPDATE
			String nuovoNome = "Soul";
			String nuovoCognome = "Goodman";
			int idDaModificare = 4;
			
			String queryUpdate = "UPDATE persona SET "
											+ "nome = ?, "
											+ "cognome = ?"
											+ "WHERE personaId = ?";
			
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(queryUpdate);
			
			ps.setString(1, nuovoNome);
			ps.setString(2, nuovoCognome);
			ps.setInt(3, idDaModificare);
			
			int affectedRows = ps.executeUpdate();
			if(affectedRows > 0) {
				System.out.println("Operazione effettuata con successo!");
			}
			else {
				System.out.println("Errore nell'update");
			}
			
			
		} catch (Exception e){
			System.out.println(e.getMessage());
		}
		
		
//		try {
//			//Corpo della prova in anticipo
//		} catch (Exception e) {
//			//Se qualcosa non va allora vado in Catch
//		}
		
		
		
//		try {
//			Connection conn = (Connection) dataSource.getConnection();
//			System.out.println("Connesso!");
//			
//			//INSERT
//			
//			//SELECT 
//			
//			//DELETE
//			
//			//UPDATE
//			
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//		}
		
	}

}

/*
DROP DATABASE IF EXISTS Rubrica;
CREATE DATABASE Rubrica;
USE Rubrica;
-- DDL

CREATE TABLE persona (
	personaId INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	nome VARCHAR(250),
    cognome VARCHAR(250),
    telefono VARCHAR(25),
    codice_fiscale VARCHAR(16) UNIQUE NOT NULL
);
*/
