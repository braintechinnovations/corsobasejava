package corso.lez11.mysql.c1_crud_avanazate;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		
		GestionePersona gestore = new GestionePersona();
		
		//Inserimento di nuove persone
//		Persona giovanni = new Persona();
//		giovanni.setPer_nome("Giovanni");
//		giovanni.setPer_cogn("Pace");
//		giovanni.setPer_tele("123456");
//		giovanni.setPer_codf("PCAGNN");
//		
//		gestore.inserisciPersona(giovanni);
//		System.out.println(giovanni.toString());		//In output ottengo LO STESSO OGGETTO ma con il campo ID popolato!
		
//		Persona mario = new Persona();
//		mario.setPer_nome("Mario");
//		mario.setPer_cogn("Rossi");
//		mario.setPer_tele("987654");
//		mario.setPer_codf("MARRRS");
//		
//		gestore.inserisciPersona(mario);
//		System.out.println(mario.toString());
		
		//Ricerco tutte le persone
//		ArrayList<Persona> risultato = gestore.cercaTuttePersone();
//		gestore.stampaRisultati(risultato);
		
//		Persona gio = gestore.cercaPerId(6);
//		System.out.println(gio.toString());
//		System.out.println(gestore.cercaPerId(7).toString());
		
//		if(gestore.eliminaPersona(gio)) {
//			System.out.println("Persona eliminata!");
//		}
//		else {
//			System.out.println("Errore di eliminazione");
//		}
		
//		boolean risultato = gestore.eliminaPersona(gio);
//		if(risultato) {
//			System.out.println("Persona eliminata!");
//		}
//		else {
//			System.out.println("Errore di eliminazione");
//		}
		
//		Persona gio = gestore.cercaPerId(7);
//		if(gio != null) {
//			System.out.println(gio.toString());
//		}
//		else {
//			System.out.println("Persona non trovata!");
//		}
		
		Persona mar = gestore.cercaPerId(7);
		
		if(mar != null) {
			mar.setPer_nome("Mario");
			mar.setPer_tele("963258687687647632648762387462836478628376428376437286478362648268623876426842736487236487623");
			
			if(gestore.modificaPersona(mar))
				System.out.println("Operazione effettuata con successo!");
			else
				System.out.println("Errore di modifica!");
		}
		
	}
	
}
